package com.osmanyasirinan.sunitohumlama.utilities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;

import java.util.List;

public class HSSpinnerListAdapter extends BaseAdapter {

    private List<HayvanSahibi> list;
    private static LayoutInflater inflater = null;

    public HSSpinnerListAdapter(Context context, List<HayvanSahibi> list) {
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;

        if (v == null)
            v = inflater.inflate(R.layout.row_hsspinnerlist, viewGroup, false);

        TextView hsName = v.findViewById(R.id.HSSpinnerListRowHSName);
        TextView hsLoc = v.findViewById(R.id.HSSpinnerListRowHsLoc);

        hsName.setText(list.get(i).isim);

        String loc = list.get(i).mahallekoy + " (" + list.get(i).province + "/" + list.get(i).town + ")";
        hsLoc.setText(loc);

        return v;
    }

}
