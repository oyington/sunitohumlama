package com.osmanyasirinan.sunitohumlama.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    // SHARED PREFERENCES
    public static final String SHARED_PREFERENCES = "prefs";
    public static final String PREFS_USER_PROVINCE = "province";

    // MAINACTIVITY FRAGMENTS
    public static final String FRAGMENT_HOME = "fragment_home";
    public static final String FRAGMENT_STATS = "fragment_stats";
    public static final String FRAGMENT_TOHUM = "fragment_tohum";
    public static final String FRAGMENT_SAHIPLER = "fragment_sahipler";
    public static final String FRAGMENT_SETTINGS = "fragment_settings";

    // INTENT EXTRAS
    public static final String EXTRA_HSUID = "hsuid";
    public static final String EXTRA_TOHUMLAMAID = "tohumlamaid";
    public static final String EXTRA_UT = "ut";
    public static final String EXTRA_MODE = "mode";
    public static final String EXTRA_SPECIFIED_FIELDS = "fields";
    public static final String EXTRA_SEARCH = "search";
    public static final String EXTRA_SEARCH_FIELDS = "searchfields";

    public static final int SF_HAYVAN_SAHIBI_ID = 0;
    public static final int SF_HAYVAN_ID = 1;

    public static final int UT_TOHUMLAMA_ID = 0;
    public static final int UT_HAYVAN_SAHIBI_ID = 1;
    public static final int UT_HAYVAN_ID = 2;
    public static final int UT_TOHUM_ID = 3;
    public static final int UT_TOHUMLAMA_TARIHI = 4;
    public static final int UT_BASARI_DURUMU = 5;

    public static final int SEARCH_HS = 0;
    public static final int SEARCH_HAYVAN = 1;
    public static final int SEARCH_TOHUMLAMA = 2;

    public static final int SEARCHF_HAYVAN_SAHIBI_ID = 0;
    public static final int SEARCHF_TOHUM_ID = 1;
    public static final int SEARCHF_BASLANGIC_MILLIS = 2;
    public static final int SEARCHF_BITIS_MILLIS = 3;
    public static final int SEARCHF_BASARI_DURUMU = 4;

    public static final int SEARCHF_HS_NAME = 0;
    public static final int SEARCHF_PROVINCE = 1;
    public static final int SEARCHF_TOWN = 2;
    public static final int SEARCHF_MAHALLEKOY = 3;

    public static final int SEARCHF_HAYVAN_BILGISI = 1;

    // DATABASE
    public static final String TABLO_UYELER = "uyeler";
    public static final String TABLO_HAYVAN_SAHIPLERI = "hayvan_sahipleri";
    public static final String TABLO_HAYVANLAR = "hayvanlar";
    public static final String TABLO_TOHUMLAMALAR = "tohumlamalar";
    public static final String TABLO_TOHUMLAR = "tohumlar";

    // AUTH
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PROVINCE = "province";
    public static final String ACCOUNT_TYPE = "type";
    public static final String ORGANISATION = "organisation";
    public static final String TOWN = "town";
    public static final String MAHALLEKOY = "mahallekoy";
    public static final String PASSWORD = "password";
    public static final String PASSWORD2 = "password2";
    public static final String VALID = "valid";
    public static final String INVALID = "invalid";

    public static final String HS_NAME = "hs_name";

    public static final String TOHUM_ADI = "tohum_adi";
    public static final String TOHUM_SAYISI = "tohum_sayisi";

    public static final String HAYVAN_BILGISI = "hayvan_bilgisi";

    public static final String TOHUMLAMA_TARIHI = "tohumlama_tarihi";
    public static final String ATILAN_TOHUM = "atilan_tohum";

    public static final String BASLANGIC_TARIHI = "baslangic_tarihi";
    public static final String BITIS_TARIHI = "bitis_tarihi";
    public static final String BASARI_DURUMU = "basari_durumu";

    public static final String[] BASARI_DURUMLARI = {"Belli Değil", "Başarılı", "Başarısız"};
    public static final int BELLI_DEGIL = 0;
    public static final int BASARILI = 1;
    public static final int BASARISIZ = -1;

    // STATS
    public static final String DATA_TOHUMLAMA_YILLIK = "yillik";
    public static final String DATA_TOHUMLAMA_AYLIK = "aylik";

    // YENI TOHUMLAMA ACTIVITY MODES
    public static final int MODE_NORMAL = 0;
    public static final int MODE_SPECIFIED_FIELDS = 1;
    public static final int MODE_UPDATE = 2;

    public static final String[] SEHIRLER = {"ADANA", "ADIYAMAN", "AFYONKARAHİSAR", "AKSARAY", "AMASYA", "ANKARA", "ANTALYA", "ARDAHAN", "ARTVİN", "AYDIN", "AĞRI", "BALIKESİR", "BARTIN", "BATMAN", "BAYBURT", "BOLU", "BURDUR", "BURSA", "BİLECİK", "BİNGÖL", "BİTLİS", "ÇANAKKALE", "ÇANKIRI", "ÇORUM", "DENİZLİ", "DÜZCE", "DİYARBAKIR", "EDİRNE", "ELAZIĞ", "ERZURUM", "ERZİNCAN", "ESKİŞEHİR", "GAZİANTEP", "GÜMÜŞHANE", "GİRESUN", "HAKKARİ", "HATAY", "ISPARTA", "IĞDIR", "İSTANBUL", "İZMİR", "KAHRAMANMARAŞ", "KARABÜK", "KARAMAN", "KARS", "KASTAMONU", "KAYSERİ", "KIRIKKALE", "KIRKLARELİ", "KIRŞEHİR", "KOCAELİ", "KONYA", "KÜTAHYA", "KİLİS", "MALATYA", "MANİSA", "MARDİN", "MERSİN", "MUĞLA", "MUŞ", "NEVŞEHİR", "NİĞDE", "ORDU", "OSMANİYE", "RİZE", "SAKARYA", "SAMSUN", "SİNOP", "SİVAS", "SİİRT", "ŞANLIURFA", "ŞIRNAK", "TEKİRDAĞ", "TOKAT", "TRABZON", "TUNCELİ", "UŞAK", "VAN", "YALOVA", "YOZGAT", "ZONGULDAK"};
    public static final String[] ACCOUNT_TYPES = {"Veteriner Hekim", "Çiftlik Sahibi"};

    public static final int TYPE_VETERINER = 0;
    public static final int TYPE_CIFTLIK_SAHIBI = 1;

    // TARIH FIELDS
    public static final int FIELD_DAY = 0;
    public static final int FIELD_MONTH = 1;
    public static final int FIELD_YEAR = 2;

    /**
     * Girilen e-postanın geçerli olup olmadığını döndüren metot.
     * @param text E-posta.
     * @return Geçerliyse true, değilse false.
     */
    public static boolean isEmailValid(String text) {
        String regex = "^[^@ \\t\\r\\n]+@[^@ \\t\\r\\n]+\\.[^@ \\t\\r\\n]+$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);

        return matcher.matches();
    }

    /**
     * Kayıt formundaki EditTextler için TextWatcher sağlayan metot.
     * @param editText EditText objesi.
     * @param inputType Input türü.
     * @return TextWatcher.
     */
    public static TextWatcher watcher(EditText editText, String inputType) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().trim().isEmpty())
                    setDrawables(editText, getIconID(inputType, false));
                else
                    setDrawables(editText, getIconID(inputType, true));
            }
        };
    }

    public static TextWatcher watcher(EditText editText, String inputType, String defaultValue) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().equals(defaultValue))
                    setDrawables(editText, getIconID(inputType, false));
                else
                    setDrawables(editText, getIconID(inputType, true));
            }
        };
    }

    /**
     * Kayıt formundaki EditTextler için OnFocusChangeListener sağlayan metot.
     * @param editText EditText objesi.
     * @param inputType Input türü.
     * @return OnFocusChangeListener.
     */
    public static View.OnFocusChangeListener focusChangeListener(EditText editText, String inputType) {
        return (view, b) -> {
            if (!b) {
                if (checkInput(inputType, editText.getText().toString().trim()))
                    setDrawables(editText, getIconID(inputType, true), getIconID(VALID, false));
                else
                    if (!editText.getText().toString().trim().isEmpty())
                        setDrawables(editText, getIconID(inputType, false), getIconID(INVALID, false));
                    else
                        setDrawables(editText, getIconID(inputType, true), getIconID(INVALID, false));
            }
        };
    }

    /**
     * Bir inputun geçerli olup olmadığını döndüren metot.
     * @param inputType Input türü.
     * @param input Input.
     * @return Geçerliyse true, değilse false.
     */
    public static boolean checkInput(String inputType, String input) {
        switch (inputType) {
            case HS_NAME:
            case NAME:
                return (input.length() >= 4 && input.contains(" ") && input.length() <= 24);

            case EMAIL:
                return isEmailValid(input);

            case PROVINCE:
                return Arrays.asList(SEHIRLER).contains(input);

            case ACCOUNT_TYPE:
                return Arrays.asList(ACCOUNT_TYPES).contains(input);

            case ORGANISATION:
                return (input.length() >= 3 && input.length() <= 28) || (input.length() == 0);

            case MAHALLEKOY:
                return (input.length() >= 2 && input.length() <= 50);

            case PASSWORD:
                return (input.length() >= 6 && input.length() <= 28);

            case TOHUM_ADI:
                return (input.length() >= 3 && input.length() <= 28);

            default:
                return false;
        }
    }

    /**
     * Geçersiz bir inputun hata mesajını döndüren metot.
     * @param inputType Input türü.
     * @param c Context.
     * @return Hata mesajı.
     */
    public static String getErrorMessage(String inputType, Context c) {
        switch (inputType) {
            case NAME:
                return c.getString(R.string.error_name);

            case EMAIL:
                return c.getString(R.string.error_email);

            case PROVINCE:
                return c.getString(R.string.error_province);

            case ACCOUNT_TYPE:
                return c.getString(R.string.error_account_type);

            case ORGANISATION:
                return c.getString(R.string.error_organisation);

            case TOWN:
                return c.getString(R.string.error_town);

            case MAHALLEKOY:
                return c.getString(R.string.error_mahallekoy);

            case PASSWORD:
                return c.getString(R.string.error_password);

            case PASSWORD2:
                return c.getString(R.string.error_password2);

            case HS_NAME:
                return c.getString(R.string.error_hs_name);

            case TOHUM_ADI:
                return c.getString(R.string.error_tohum_adi);

            case TOHUM_SAYISI:
                return c.getString(R.string.error_tohum_sayisi);

            case HAYVAN_BILGISI:
                return c.getString(R.string.error_hayvan_bilgisi);

            default:
                return "";
        }
    }

    /**
     * Input türüne göre icon IDsini getiren metot.
     * @param inputType Input türü.
     * @param colorful Iconunun renki olup olmayacağı.
     * @return Iconun drawable IDsi.
     */
    public static int getIconID(String inputType, boolean colorful) {
        switch (inputType) {
            case HS_NAME:
            case NAME:
                if (colorful) return R.drawable.ic_person_focused;
                else return R.drawable.ic_person;

            case EMAIL:
                if (colorful) return R.drawable.ic_email_focused;
                else return R.drawable.ic_email;

            case PROVINCE:
                if (colorful) return R.drawable.ic_location_focused;
                else return R.drawable.ic_location;

            case ACCOUNT_TYPE:
                if (colorful) return R.drawable.account_type_focused;
                else return R.drawable.account_type;

            case ORGANISATION:
                if (colorful) return R.drawable.ic_apartment_focused;
                else return R.drawable.ic_apartment;

            case TOWN:
                if (colorful) return R.drawable.ic_town_focused;
                else return R.drawable.ic_town;

            case MAHALLEKOY:
                if (colorful) return R.drawable.ic_mahallekoy_focused;
                else return R.drawable.ic_mahallekoy;

            case PASSWORD:
            case PASSWORD2:
                if (colorful) return R.drawable.ic_lock_focused;
                else return R.drawable.ic_lock;

            case ATILAN_TOHUM:
            case TOHUM_ADI:
                if (colorful) return R.drawable.bubble_focused;
                else return R.drawable.bubble;

            case HAYVAN_BILGISI:
                if (colorful) return R.drawable.animal_focused;
                else return R.drawable.animal;

            case TOHUMLAMA_TARIHI:
                if (colorful) return R.drawable.ic_date_focused;
                else return R.drawable.ic_date;

            case VALID:
                return R.drawable.ic_check;

            case INVALID:
                return R.drawable.ic_close;

            default:
                return 0;
        }
    }

    /**
     * İlçeler JSON objesini getiren metot.
     * @param c Context.
     * @return İlçeler JSON objesi.
     */
    public static JSONObject getTowns(Context c) {
        try {
            InputStreamReader isr = new InputStreamReader(c.getResources().openRawResource(R.raw.iller_ilceler));
            BufferedReader reader = new BufferedReader(isr);

            return new JSONObject(reader.readLine());
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Auth'ta alınan exceptionların hata mesajlarını döndüren metot.
     * @param e Alınan exception.
     * @param c Context.
     * @return Hata mesajı.
     */
    public static String getAuthErrorMessage(Exception e, Context c) {
        try { throw e; }
        catch (FirebaseAuthInvalidUserException exception) { exception.printStackTrace(); return c.getString(R.string.error_invalid_user); }
        catch (FirebaseAuthUserCollisionException exception) { exception.printStackTrace(); return c.getString(R.string.error_account_exists); }
        catch (FirebaseException exception) { exception.printStackTrace(); return c.getString(R.string.error_check_your_connection); }
        catch (Exception exception) { exception.printStackTrace(); return c.getString(R.string.error_account_not_created); }
    }

    /**
     * MainActivity'deki bottomNavigation'daki item idsine göre fragment ismini döndüren metot.
     * @param bottomNavItemID Item ID.
     * @return Fragment ismi.
     */
    public static String getFragmentName(int bottomNavItemID) {
        switch (bottomNavItemID) {
            case R.id.statsItem:
                return FRAGMENT_STATS;

            case R.id.tohumItem:
                return FRAGMENT_TOHUM;

            case R.id.sahiplerItem:
                return FRAGMENT_SAHIPLER;

            case R.id.settingsItem:
                return FRAGMENT_SETTINGS;

            default:
                return FRAGMENT_HOME;
        }
    }

    /**
     * SharedPreferences'taki user province değerini güncelleyen metot.
     * @param c Context.
     * @param province Şehir adı.
     */
    public static void updateUserProvince(Context c, String province) {
        SharedPreferences prefs = c.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREFS_USER_PROVINCE, province);
        editor.apply();
    }

    /**
     * SharedPreferences'taki user province değerini getiren metot.
     * @param c Context.
     * @return Şehir adı.
     */
    public static String getUserProvince(Context c) {
        SharedPreferences prefs = c.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return prefs.getString(PREFS_USER_PROVINCE, null);
    }

    public static String getDateString(int year, int month, int day) {
        return addZero(day) + "." + addZero(month) + "." + year;
    }

    /**
     * Bir tarih bölümünü gerekliyse 0 ekleyerek String'e çeviren metot.
     * @param number Tarih bölümü (ay veya gün).
     * @return Hazır String.
     */
    public static String addZero(int number) {
        return (String.valueOf(number).length() == 1) ? "0" + number : String.valueOf(number);
    }

    public static int getField(String str, int field) {
        if (field == FIELD_DAY)
            return Integer.parseInt(String.valueOf(str.charAt(0)) + str.charAt(1));
        else if (field == FIELD_MONTH)
            return Integer.parseInt(String.valueOf(str.charAt(3)) + str.charAt(4));
        else
            return Integer.parseInt(String.valueOf(str.charAt(6)) + str.charAt(7) + str.charAt(8) + str.charAt(9));
    }

    public static void setDrawables(TextView tv, int drawableLeft) {
        tv.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, 0 , 0, 0);
    }

    public static void setDrawables(TextView tv, int drawableLeft, int drawableRight) {
        tv.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, 0 , drawableRight, 0);
    }

    public static String getMonthStr(int month, Context c) {
        switch (month) {
            case 1:
                return c.getString(R.string.subat);

            case 2:
                return c.getString(R.string.mart);

            case 3:
                return c.getString(R.string.nisan);

            case 4:
                return c.getString(R.string.mayis);

            case 5:
                return c.getString(R.string.haziran);

            case 6:
                return c.getString(R.string.temmuz);

            case 7:
                return c.getString(R.string.agustos);

            case 8:
                return c.getString(R.string.eylul);

            case 9:
                return c.getString(R.string.ekim);

            case 10:
                return c.getString(R.string.kasim);

            case 11:
                return c.getString(R.string.aralik);

            default:
                return c.getString(R.string.ocak);
        }
    }

    public static String getDayStr(String dateStr, Context context) {
        Calendar c = Calendar.getInstance();

        if (c.get(Calendar.DAY_OF_MONTH) == getField(dateStr, FIELD_DAY) &&
            c.get(Calendar.MONTH) + 1 == getField(dateStr, FIELD_MONTH) &&
            c.get(Calendar.YEAR) == getField(dateStr, FIELD_YEAR))
            return context.getString(R.string.today);
        else {
            c.add(Calendar.DAY_OF_YEAR, -1);
            if (c.get(Calendar.DAY_OF_MONTH) == getField(dateStr, FIELD_DAY) &&
                    c.get(Calendar.MONTH) + 1 == getField(dateStr, FIELD_MONTH) &&
                    c.get(Calendar.YEAR) == getField(dateStr, FIELD_YEAR)) {
                return context.getString(R.string.yesterday);
            }else if (c.get(Calendar.YEAR) == getField(dateStr, FIELD_YEAR))
                return getField(dateStr, FIELD_DAY) + " " + getMonthStr(getField(dateStr, FIELD_MONTH) - 1, context);
            else
                return getField(dateStr, FIELD_DAY) + " " + getMonthStr(getField(dateStr, FIELD_MONTH) - 1, context) + " " + getField(dateStr, FIELD_YEAR);
        }
    }

    public static long convertToMillis(String date) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(getField(date, FIELD_YEAR), getField(date, FIELD_MONTH) - 1, getField(date, FIELD_DAY), 0, 0);
        return calendar.getTimeInMillis();
    }

    /**
     * YeniHayvanDialog ile HSSpinnerDialog arasındaki iletişimi sağlayan interface.
     */
    public interface HSListener {
        void onHSSelected(HayvanSahibi selected);
    }

}
