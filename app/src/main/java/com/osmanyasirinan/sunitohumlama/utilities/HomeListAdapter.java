package com.osmanyasirinan.sunitohumlama.utilities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.Hayvan;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;
import com.osmanyasirinan.sunitohumlama.data.Tohumlama;

import java.util.List;

public class HomeListAdapter extends BaseAdapter {

    private List<Tohumlama> list;
    private static LayoutInflater inflater = null;
    private Context context;

    public HomeListAdapter(Context context, List<Tohumlama> list) {
        this.list = list;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;

        if (v == null)
            v = inflater.inflate(R.layout.tohumlama, viewGroup, false);

        TextView hayvanSahibi = v.findViewById(R.id.tohumlamaHayvanSahibi);
        TextView hayvanBilgisi = v.findViewById(R.id.tohumlamaHayvanBilgisi);
        ImageView basariDurumu = v.findViewById(R.id.tohumlamaBasariDurumu);

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVANLAR);
        ref.child(list.get(i).getHayvanID()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Hayvan h = dataSnapshot.getValue(Hayvan.class);

                if (h != null) {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVAN_SAHIPLERI);
                    ref.child(h.hayvanSahibiID).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            HayvanSahibi hs = dataSnapshot.getValue(HayvanSahibi.class);

                            if (hs != null) {
                                String str = hs.isim + " (" + Utils.getDayStr(list.get(i).getTohumlamaTarihi(), context) + ")";
                                hayvanSahibi.setText(str);
                                hayvanBilgisi.setText(h.hayvanBilgisi);

                                if (list.get(i).getBasariDurumu() == Utils.BASARILI)
                                    basariDurumu.setImageResource(R.drawable.ic_check);
                                else if (list.get(i).getBasariDurumu() == Utils.BASARISIZ)
                                    basariDurumu.setImageResource(R.drawable.ic_close);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return v;
    }
}
