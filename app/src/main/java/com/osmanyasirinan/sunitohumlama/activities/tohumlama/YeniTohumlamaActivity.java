package com.osmanyasirinan.sunitohumlama.activities.tohumlama;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.Hayvan;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;
import com.osmanyasirinan.sunitohumlama.data.Tohum;
import com.osmanyasirinan.sunitohumlama.data.Tohumlama;
import com.osmanyasirinan.sunitohumlama.dialogs.main.HSSelectDialog;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class YeniTohumlamaActivity extends AppCompatActivity implements Utils.HSListener {

    private EditText HSET, tohumlamaTarihiET;
    private Spinner basariDurumuSpinner, hayvanSpinner, atilanTohum;
    private Button send;
    private TextView title;
    private FirebaseAuth mAuth;

    private List<Hayvan> hayvanlar;
    private List<Tohum> tohumlar;
    private List<HayvanSahibi> hayvanSahipleri;

    private int MODE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yeni_tohumlama);

        if (getIntent() != null) {
            MODE = getIntent().getIntExtra(Utils.EXTRA_MODE, Utils.MODE_NORMAL);
        }else MODE = Utils.MODE_NORMAL;

        HSET = findViewById(R.id.yeniTohumlamaHS);
        hayvanSpinner = findViewById(R.id.yeniTohumlamaHayvan);
        tohumlamaTarihiET = findViewById(R.id.tohumlamaTarihi);
        atilanTohum = findViewById(R.id.yeniTohumlamaTohum);

        title = findViewById(R.id.yeniTohumlamaTitle);

        basariDurumuSpinner = findViewById(R.id.yeniTohumlamaBasariDurumu);

        send = findViewById(R.id.yeniTohumlamaBtn);

        HSET.addTextChangedListener(Utils.watcher(HSET, Utils.HS_NAME));
        tohumlamaTarihiET.addTextChangedListener(Utils.watcher(tohumlamaTarihiET, Utils.TOHUMLAMA_TARIHI));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner_centered_text, Utils.BASARI_DURUMLARI);
        basariDurumuSpinner.setAdapter(adapter);

        send.setOnClickListener(view -> send());

        hayvanSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    hayvanSpinner.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.hayvan_spinner, null));
                else
                    hayvanSpinner.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.hayvan_spinner_focused, null));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
        atilanTohum.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    atilanTohum.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.tohum_spinner, null));
                else
                    atilanTohum.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.tohum_spinner_focused, null));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        tohumlamaTarihiET.setOnClickListener(view -> {
            Calendar c = Calendar.getInstance();

            String str = tohumlamaTarihiET.getText().toString();
            int year, month, day;

            if (str.isEmpty()) {
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
            }else {
                year = Utils.getField(str, Utils.FIELD_YEAR);
                month = Utils.getField(str, Utils.FIELD_MONTH);
                day = Utils.getField(str, Utils.FIELD_YEAR);
            }

            DatePickerDialog dpd = new DatePickerDialog(this, (datePicker, y, m, d) -> tohumlamaTarihiET.setText(Utils.getDateString(y, m + 1, d)), year, month, day);
            dpd.show();
        });
    }

    private void send() {
        if (mAuth.getCurrentUser() != null) {
            String errorMessage = checkInputs();

            if (errorMessage == null) {
                Tohumlama t = new Tohumlama(
                        mAuth.getCurrentUser().getUid(),
                        hayvanlar.get(hayvanSpinner.getSelectedItemPosition() - 1).id,
                        tohumlamaTarihiET.getText().toString(),
                        tohumlar.get(atilanTohum.getSelectedItemPosition() - 1).id,
                        getBasariDurumu(),
                        hayvanlar.get(hayvanSpinner.getSelectedItemPosition() - 1).hayvanSahibiID
                );

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAMALAR);

                if (MODE == Utils.MODE_UPDATE)
                    ref.child(getIntent().getStringArrayListExtra(Utils.EXTRA_UT).get(Utils.UT_TOHUMLAMA_ID)).setValue(t);
                else {
                    ref.push().setValue(t);
                    DatabaseReference ref2 = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAR);
                    ref2.child(tohumlar.get(atilanTohum.getSelectedItemPosition() - 1).id).setValue(
                            new Tohum(
                                    tohumlar.get(atilanTohum.getSelectedItemPosition() - 1).isim,
                                    tohumlar.get(atilanTohum.getSelectedItemPosition() - 1).sayi -1,
                                    mAuth.getCurrentUser().getUid()
                            )
                    );
                }
                finish();
            } else Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }

    private int getBasariDurumu() {
        if (basariDurumuSpinner.getSelectedItemPosition() == 0 || basariDurumuSpinner.getSelectedItemPosition() == 1)
            return basariDurumuSpinner.getSelectedItemPosition();
        else
            return -1;
    }

    private String checkInputs() {
        if (!HSET.getText().toString().isEmpty())
            if (hayvanSpinner.getSelectedItemPosition() != 0)
                if (!tohumlamaTarihiET.getText().toString().isEmpty())
                    if (atilanTohum.getSelectedItemPosition() != 0)
                        return null;
                    else
                        return getString(R.string.error_select_tohum);
                else
                    return getString(R.string.error_tohumlama_tarihi);
            else
                return getString(R.string.error_select_hayvan);
        else
            return getString(R.string.error_select_hs);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();

        hayvanSahipleri = new ArrayList<>();

        if (mAuth.getCurrentUser() != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVAN_SAHIPLERI);
            Query query = ref.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        HayvanSahibi hs = ds.getValue(HayvanSahibi.class);

                        if (hs != null) {
                            hs.uid = ds.getKey();
                            hayvanSahipleri.add(hs);
                        }
                    }

                    HSET.setOnClickListener(view -> new HSSelectDialog(getSupportFragmentManager(), hayvanSahipleri, YeniTohumlamaActivity.this).createDialog());

                    if (MODE == Utils.MODE_SPECIFIED_FIELDS || MODE == Utils.MODE_UPDATE)
                        fill();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            tohumlar = new ArrayList<>();
            DatabaseReference ref2 = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAR);
            Query query2 = ref2.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
            query2.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        Tohum t = ds.getValue(Tohum.class);

                        if (t != null) {
                            t.id = ds.getKey();
                            tohumlar.add(t);
                        }
                    }

                    List<String> tohumIsimleri = new ArrayList<>();
                    tohumIsimleri.add(getString(R.string.atilan_tohum));

                    for (Tohum t : tohumlar)
                        tohumIsimleri.add(t.isim);

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(YeniTohumlamaActivity.this, R.layout.spinner, tohumIsimleri);
                    atilanTohum.setAdapter(adapter);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    @Override
    public void onHSSelected(HayvanSahibi selected) {
        HSET.setText(selected.isim);

        hayvanlar = new ArrayList<>();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVANLAR);
        Query query = ref.orderByChild("hayvanSahibiID").equalTo(selected.uid);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Hayvan h = ds.getValue(Hayvan.class);

                    if (h != null) {
                        h.id = ds.getKey();
                        hayvanlar.add(h);
                    }
                }

                List<String> hayvanBilgisiList = new ArrayList<>();
                hayvanBilgisiList.add(getString(R.string.hayvan));

                for (Hayvan h : hayvanlar)
                    hayvanBilgisiList.add(h.hayvanBilgisi);

                ArrayAdapter<String> adapter = new ArrayAdapter<>(YeniTohumlamaActivity.this, R.layout.spinner, hayvanBilgisiList);
                hayvanSpinner.setAdapter(adapter);

                if (MODE == Utils.MODE_UPDATE || MODE == Utils.MODE_SPECIFIED_FIELDS)
                    fill2();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void fill() {
        if (MODE == Utils.MODE_SPECIFIED_FIELDS) {
            ArrayList<String> fields = getIntent().getStringArrayListExtra(Utils.EXTRA_SPECIFIED_FIELDS);

            HayvanSahibi hs = null;

            for (HayvanSahibi hs1 : hayvanSahipleri) {
                if (hs1.uid.equals(fields.get(Utils.SF_HAYVAN_SAHIBI_ID)))
                    hs = hs1;
            }

            if (hs != null)
                onHSSelected(hs);
        }else if (MODE == Utils.MODE_UPDATE) {
            title.setText(R.string.tohumlamayi_duzenle);
            ArrayList<String> fields = getIntent().getStringArrayListExtra(Utils.EXTRA_UT);

            HayvanSahibi hs = null;

            for (HayvanSahibi hs1 : hayvanSahipleri) {
                if (hs1.uid.equals(fields.get(Utils.UT_HAYVAN_SAHIBI_ID)))
                    hs = hs1;
            }

            if (hs != null)
                onHSSelected(hs);
        }
    }

    private void fill2() {
        if (MODE == Utils.MODE_SPECIFIED_FIELDS) {
            ArrayList<String> list = getIntent().getStringArrayListExtra(Utils.EXTRA_SPECIFIED_FIELDS);

            Hayvan h = null;

            for (Hayvan h1 : hayvanlar) {
                if (h1.id.equals(list.get(Utils.SF_HAYVAN_ID)))
                    h = h1;
            }

            if (h != null)
                hayvanSpinner.setSelection(hayvanlar.indexOf(h) + 1);
        }else if (MODE == Utils.MODE_UPDATE) {
            ArrayList<String> list = getIntent().getStringArrayListExtra(Utils.EXTRA_UT);

            Hayvan h = null;
            Tohum t = null;
            String tarih = list.get(Utils.UT_TOHUMLAMA_TARIHI);
            int basariDurumu = Integer.parseInt(list.get(Utils.UT_BASARI_DURUMU));

            for (Hayvan h1 : hayvanlar) {
                if (h1.id.equals(list.get(Utils.UT_HAYVAN_ID)))
                    h = h1;
            }

            for (Tohum t1 : tohumlar) {
                if (t1.id.equals(list.get(Utils.UT_TOHUM_ID)))
                    t = t1;
            }

            if (h != null && t != null) {
                hayvanSpinner.setSelection(hayvanlar.indexOf(h) + 1);
                this.tohumlamaTarihiET.setText(tarih);
                atilanTohum.setSelection(tohumlar.indexOf(t) + 1);
                if (basariDurumu >= 0)
                    this.basariDurumuSpinner.setSelection(basariDurumu);
                else
                    this.basariDurumuSpinner.setSelection(2);
            }
        }
    }

}