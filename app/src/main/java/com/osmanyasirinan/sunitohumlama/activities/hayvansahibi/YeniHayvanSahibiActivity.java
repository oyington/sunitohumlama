package com.osmanyasirinan.sunitohumlama.activities.hayvansahibi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class YeniHayvanSahibiActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText nameET, mahallekoyET;
    private Spinner spinner;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yeni_hayvan_sahibi);

        nameET = findViewById(R.id.hsNameET);
        mahallekoyET = findViewById(R.id.hsMahallekoyET);

        spinner = findViewById(R.id.hsTownSp);

        button = findViewById(R.id.hsBtn);

        ImageView back = findViewById(R.id.backInYeniHS);
        back.setOnClickListener(view -> finish());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {
            nameET.addTextChangedListener(Utils.watcher(nameET, Utils.HS_NAME));
            mahallekoyET.addTextChangedListener(Utils.watcher(mahallekoyET, Utils.MAHALLEKOY));

            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_UYELER).child(mAuth.getCurrentUser().getUid());

            String province = Utils.getUserProvince(this);

            try {
                JSONObject towns = Utils.getTowns(YeniHayvanSahibiActivity.this);
                if (towns != null && province != null) {
                    JSONArray jsonArray = (JSONArray) towns.get(province);
                    List<String> list = new ArrayList<>();
                    list.add(getString(R.string.town));
                    for (int i = 0; i < jsonArray.length(); i++)
                        list.add(jsonArray.get(i).toString());
                    String[] ilceler = list.toArray(new String[0]);
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(YeniHayvanSahibiActivity.this, R.layout.spinner, ilceler);
                    spinner.setAdapter(adapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i == 0)
                                spinner.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.town_spinner, null));
                            else
                                spinner.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.town_spinner_focused, null));
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });
                    button.setOnClickListener(view -> {
                        String errorMessage = checkInputs();
                        if (errorMessage == null)
                            insert(new HayvanSahibi(
                                    mAuth.getCurrentUser().getUid(),
                                    nameET.getText().toString().trim(),
                                    province,
                                    spinner.getSelectedItem().toString(),
                                    mahallekoyET.getText().toString().trim()
                            ));
                        else Toast.makeText(YeniHayvanSahibiActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else finish();
    }

    private void insert(HayvanSahibi hs) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

        String key = ref.child(Utils.TABLO_HAYVAN_SAHIPLERI).push().getKey();

        if (key != null) {
            ref.child(Utils.TABLO_HAYVAN_SAHIPLERI).child(key).setValue(hs);

            Intent i = new Intent(YeniHayvanSahibiActivity.this, HayvanSahibiDetayActivity.class);
            i.putExtra(Utils.EXTRA_HSUID, key);
            startActivity(i);
            finish();
        }
    }

    private String checkInputs() {
        Context c = this;

        if (Utils.checkInput(Utils.HS_NAME, nameET.getText().toString().trim()))
            if (spinner.getSelectedItemPosition() != 0)
                if (Utils.checkInput(Utils.MAHALLEKOY, mahallekoyET.getText().toString().trim()))
                    return null;
                else
                    return Utils.getErrorMessage(Utils.MAHALLEKOY, c);
            else
                return Utils.getErrorMessage(Utils.TOWN, c);
        else
            return Utils.getErrorMessage(Utils.HS_NAME, c);
    }
}