package com.osmanyasirinan.sunitohumlama.activities.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.auth.LoginActivity;
import com.osmanyasirinan.sunitohumlama.fragments.main.HomeFragment;
import com.osmanyasirinan.sunitohumlama.fragments.main.SahiplerFragment;
import com.osmanyasirinan.sunitohumlama.fragments.main.SettingsFragment;
import com.osmanyasirinan.sunitohumlama.fragments.main.StatsFragment;
import com.osmanyasirinan.sunitohumlama.fragments.main.TohumFragment;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

public class MainActivity extends AppCompatActivity  {

    private FirebaseAuth mAuth;
    private String currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        BottomNavigationView navView = findViewById(R.id.bottomNavView);
        navView.setOnNavigationItemSelectedListener(navSelectedListener);
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser user = mAuth.getCurrentUser();

        if (user == null) {
            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }else updateUI(user);
    }

    @Override
    protected void onResume() {
        super.onResume();

        selectFragment(fragment((currentFragment == null) ? "" : currentFragment));
    }

    private void updateUI(FirebaseUser user) {

    }

    private final BottomNavigationView.OnNavigationItemSelectedListener navSelectedListener = item -> {
        currentFragment = Utils.getFragmentName(item.getItemId());
        Fragment selected;

        if (item.getItemId() == R.id.statsItem)
            selected = new StatsFragment();
        else if (item.getItemId() == R.id.tohumItem)
            selected = new TohumFragment();
        else if (item.getItemId() == R.id.sahiplerItem)
            selected = new SahiplerFragment();
        else if (item.getItemId() == R.id.settingsItem)
            selected = new SettingsFragment();
        else
            selected = new HomeFragment();

        selectFragment(selected);
        return true;
    };

    private void selectFragment(Fragment f) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, f).commit();
    }

    private Fragment fragment(String fragmentName) {
        switch (fragmentName) {
            case Utils.FRAGMENT_STATS:
                return new StatsFragment();

            case Utils.FRAGMENT_TOHUM:
                return new TohumFragment();

            case Utils.FRAGMENT_SAHIPLER:
                return new SahiplerFragment();

            case Utils.FRAGMENT_SETTINGS:
                return new SettingsFragment();

            default:
                return new HomeFragment();
        }
    }

}