package com.osmanyasirinan.sunitohumlama.activities.hayvansahibi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.Hayvan;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;
import com.osmanyasirinan.sunitohumlama.data.Tohumlama;
import com.osmanyasirinan.sunitohumlama.dialogs.hayvan.HayvanDetayDialog;
import com.osmanyasirinan.sunitohumlama.dialogs.hayvan.YeniHayvanDialog;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class HayvanSahibiDetayActivity extends AppCompatActivity {

    private String uid;
    private TextView name, location;
    private ImageView edit, delete, addHayvan, back;
    private String dialogTitle;
    private ListView hayvanlarLV;
    private List<Hayvan> hayvanlar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hayvan_sahibi_detay);

        uid = getIntent().getStringExtra(Utils.EXTRA_HSUID);
        name = findViewById(R.id.HSDetayNameTV);
        location = findViewById(R.id.HSDetayLocationTV);

        edit = findViewById(R.id.HSDetayEditBtn);
        delete = findViewById(R.id.HSDetayDeleteBtn);
        addHayvan = findViewById(R.id.HSDetayHayvanEkleBtn);
        back = findViewById(R.id.backInHSDetay);
        back.setOnClickListener(view -> finish());

        hayvanlarLV = findViewById(R.id.HSDetayHayvanlarLV);

        edit.setOnClickListener(view -> {
            Intent i = new Intent(HayvanSahibiDetayActivity.this, HayvanSahibiEditActivity.class);
            i.putExtra(Utils.EXTRA_HSUID, uid);
            startActivity(i);
        });

        delete.setOnClickListener(view -> {
            AlertDialog.Builder dialog = createDialog();
            dialog.show();
        });

        hayvanlar = new ArrayList<>();

        hayvanlarLV.setOnItemClickListener((adapterView, view, i, l) -> new HayvanDetayDialog(getSupportFragmentManager(), hayvanlar.get(i)).createDialog());
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (uid == null)
            finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVAN_SAHIPLERI);
        ref.child(uid).addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HayvanSahibi hs = dataSnapshot.getValue(HayvanSahibi.class);
                if (hs != null) {
                    fill(hs);
                    dialogTitle = hs.isim;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void fill(HayvanSahibi hs) {
        name.setText(hs.isim);
        String loc = hs.town + " • " + hs.mahallekoy;
        location.setText(loc);

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVANLAR);
        Query query = ref.orderByChild("hayvanSahibiID").equalTo(uid);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> list = new ArrayList<>();

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Hayvan h = ds.getValue(Hayvan.class);

                    if (h != null) {
                        list.add(h.hayvanBilgisi);
                        h.id = ds.getKey();
                        hayvanlar.add(h);
                    }
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(HayvanSahibiDetayActivity.this, R.layout.hayvan, R.id.hayvanTV, list);
                hayvanlarLV.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        hs.uid = uid;
        addHayvan.setOnClickListener(view -> new YeniHayvanDialog(getSupportFragmentManager(), hs).createDialog());
    }

    private void fill(HayvanSahibi hs, Tohumlama sonTohumlama) {
        fill(hs);
        // TODO: sonTohumlama
    }

    private AlertDialog.Builder createDialog() {
        return new AlertDialog.Builder(this)
                .setTitle(dialogTitle)
                .setMessage(getString(R.string.sure_delete_hs))
                .setPositiveButton(getString(R.string.yes), (dialogInterface, i) -> deleteHS())
                .setNegativeButton(getString(R.string.no), null);
    }

    private void deleteHS() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVAN_SAHIPLERI);
        ref.child(uid).removeValue();
        finish();
    }

}