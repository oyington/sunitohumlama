package com.osmanyasirinan.sunitohumlama.activities.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.activities.hayvansahibi.HayvanSahibiDetayActivity;
import com.osmanyasirinan.sunitohumlama.activities.tohumlama.TohumlamaDetayActivity;
import com.osmanyasirinan.sunitohumlama.activities.tohumlama.YeniTohumlamaActivity;
import com.osmanyasirinan.sunitohumlama.data.Hayvan;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;
import com.osmanyasirinan.sunitohumlama.data.Tohumlama;
import com.osmanyasirinan.sunitohumlama.dialogs.hayvan.HayvanDetayDialog;
import com.osmanyasirinan.sunitohumlama.utilities.HomeListAdapter;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class FilteredListActivity extends AppCompatActivity {

    private int MODE;
    private ArrayList<String> LIST;
    private ImageView back;
    private ListView listView;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtered_list);

        back = findViewById(R.id.backInFilteredList);
        listView = findViewById(R.id.filteredLv);

        MODE = getIntent().getIntExtra(Utils.EXTRA_SEARCH, -1);
        LIST = getIntent().getStringArrayListExtra(Utils.EXTRA_SEARCH_FIELDS);

        back.setOnClickListener(view -> finish());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();

        fillList();
    }

    private void fillList() {
        switch (MODE) {
            case Utils.SEARCH_HS:
                if (mAuth.getCurrentUser() != null) {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVAN_SAHIPLERI);
                    Query query = ref.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
                    query.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            List<String> list = new ArrayList<>();
                            List<HayvanSahibi> hayvanSahipleri = new ArrayList<>();

                            for (DataSnapshot ds : snapshot.getChildren()) {
                                HayvanSahibi hs = ds.getValue(HayvanSahibi.class);

                                if (hs != null) {
                                    hs.uid = ds.getKey();

                                    if (!LIST.get(Utils.SEARCHF_HS_NAME).isEmpty()) {
                                        if (!hs.isim.toLowerCase().contains(LIST.get(Utils.SEARCHF_HS_NAME).toLowerCase()))
                                            continue;
                                    }

                                    if (!LIST.get(Utils.SEARCHF_PROVINCE).isEmpty()) {
                                        if (!hs.province.equals(LIST.get(Utils.SEARCHF_PROVINCE)))
                                            continue;
                                    }

                                    if (!LIST.get(Utils.SEARCHF_TOWN).isEmpty()) {
                                        if (!hs.town.equals(LIST.get(Utils.SEARCHF_TOWN)))
                                            continue;
                                    }

                                    if (!LIST.get(Utils.SEARCHF_MAHALLEKOY).isEmpty()) {
                                        if (!hs.mahallekoy.toLowerCase().contains(LIST.get(Utils.SEARCHF_MAHALLEKOY).toLowerCase()))
                                            continue;
                                    }

                                    hayvanSahipleri.add(hs);
                                    list.add(hs.isim);
                                }
                            }

                            listView.setAdapter(new ArrayAdapter<>(FilteredListActivity.this, R.layout.hayvan_sahibi, R.id.hayvanSahibiTV, list));
                            listView.setOnItemClickListener((adapterView, view, i, l) -> {
                                Intent in = new Intent(FilteredListActivity.this, HayvanSahibiDetayActivity.class);
                                in.putExtra(Utils.EXTRA_HSUID, hayvanSahipleri.get(i).uid);
                                startActivity(in);
                            });
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {}
                    });
                }
                break;

            case Utils.SEARCH_HAYVAN:
                if (mAuth.getCurrentUser() != null) {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVANLAR);
                    Query query = ref.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
                    query.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            List<String> list = new ArrayList<>();
                            List<Hayvan> hayvanlar = new ArrayList<>();

                            for (DataSnapshot ds : snapshot.getChildren()) {
                                Hayvan h = ds.getValue(Hayvan.class);

                                if (h != null) {
                                    h.id = ds.getKey();

                                    if (!LIST.get(Utils.SEARCHF_HAYVAN_SAHIBI_ID).isEmpty()) {
                                        if (!h.hayvanSahibiID.equals(LIST.get(Utils.SEARCHF_HAYVAN_SAHIBI_ID)))
                                            continue;
                                    }

                                    hayvanlar.add(h);
                                    list.add(h.hayvanBilgisi);
                                }
                            }

                            List<String> finalList = new ArrayList<>();

                            if (!LIST.get(Utils.SEARCHF_HAYVAN_BILGISI).isEmpty()) {
                                for (String str : list) {
                                    if (str.toLowerCase().contains(LIST.get(Utils.SEARCHF_HAYVAN_BILGISI).toLowerCase()))
                                        finalList.add(str);
                                }
                            }

                            listView.setAdapter(new ArrayAdapter<>(FilteredListActivity.this, R.layout.hayvan, R.id.hayvanTV, finalList));
                            listView.setOnItemClickListener(((adapterView, view, i, l) -> new HayvanDetayDialog(getSupportFragmentManager(), hayvanlar.get(i)).createDialog()));
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {}
                    });
                }
                break;

            case Utils.SEARCH_TOHUMLAMA:
                if (mAuth.getCurrentUser() != null) {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAMALAR);
                    Query query = ref.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
                    query.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            List<Tohumlama> list = new ArrayList<>();
                            for (DataSnapshot ds : snapshot.getChildren()) {
                                Tohumlama t = ds.getValue(Tohumlama.class);

                                if (t != null) {
                                    t.setId(ds.getKey());

                                    if (!LIST.get(Utils.SEARCHF_HAYVAN_SAHIBI_ID).isEmpty()) {
                                        if (!t.getHayvanSahibiID().equals(LIST.get(Utils.SEARCHF_HAYVAN_SAHIBI_ID)))
                                            continue;
                                    }

                                    if (!LIST.get(Utils.SEARCHF_TOHUM_ID).isEmpty()) {
                                        if (!t.getAtilanTohumID().equals(LIST.get(Utils.SEARCHF_TOHUM_ID)))
                                            continue;
                                    }

                                    if (!LIST.get(Utils.SEARCHF_BASARI_DURUMU).isEmpty()) {
                                        if (t.getBasariDurumu() != Integer.parseInt(LIST.get(Utils.SEARCHF_BASARI_DURUMU)))
                                            continue;
                                    }


                                    if (!LIST.get(Utils.SEARCHF_BASLANGIC_MILLIS).isEmpty()) {
                                        if (Long.parseLong(LIST.get(Utils.SEARCHF_BASLANGIC_MILLIS)) > Utils.convertToMillis(t.getTohumlamaTarihi()))
                                            continue;
                                    }

                                    if (!LIST.get(Utils.SEARCHF_BITIS_MILLIS).isEmpty()) {
                                        if (Long.parseLong(LIST.get(Utils.SEARCHF_BITIS_MILLIS)) < Utils.convertToMillis(t.getTohumlamaTarihi()))
                                            continue;
                                    }

                                    list.add(t);
                                }
                                HomeListAdapter adapter = new HomeListAdapter(FilteredListActivity.this, list);
                                listView.setAdapter(adapter);
                                listView.setOnItemClickListener((adapterView, view, i, l) -> {
                                    Intent in = new Intent(FilteredListActivity.this, TohumlamaDetayActivity.class);
                                    in.putExtra(Utils.EXTRA_TOHUMLAMAID, list.get(i).getId());
                                    startActivity(in);
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                        }
                    });
                }
                break;
        }
    }

}