package com.osmanyasirinan.sunitohumlama.activities.tohumlama;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.Hayvan;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;
import com.osmanyasirinan.sunitohumlama.data.Tohum;
import com.osmanyasirinan.sunitohumlama.data.Tohumlama;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import java.util.ArrayList;

public class TohumlamaDetayActivity extends AppCompatActivity {

    private String id;

    private TextView HS, hayvan, tarih, tohum;
    private ImageView backBtn, editBtn, deleteBtn, yesBtn, noBtn, basariDurumu;
    private LinearLayout basariliOlduMu, basariDurumuContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tohumlama_detay);

        id = getIntent().getStringExtra(Utils.EXTRA_TOHUMLAMAID);

        HS = findViewById(R.id.tohumlamaDetayHS);
        hayvan = findViewById(R.id.tohumlamaDetayHayvan);
        tarih = findViewById(R.id.tohumlamaDetayTarih);
        tohum = findViewById(R.id.tohumlamaDetayTohum);

        backBtn = findViewById(R.id.backInTohumlamaDetay);
        editBtn = findViewById(R.id.tohumlamaDetayEditBtn);
        deleteBtn = findViewById(R.id.tohumlamaDetayDeleteBtn);
        yesBtn = findViewById(R.id.tohumlamaDetayYesBtn);
        noBtn = findViewById(R.id.tohumlamaDetayNoBtn);
        basariDurumu = findViewById(R.id.tohumlamaDetayBasariDurumu);

        basariliOlduMu = findViewById(R.id.tohumlamaDetayBasariliOlduMu);
        basariDurumuContainer = findViewById(R.id.tohumlamaDetayBasariDurumuContainer);

        backBtn.setOnClickListener(view -> finish());
        deleteBtn.setOnClickListener(view -> delete());
        yesBtn.setOnClickListener(view -> {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAMALAR);
            ref.child(id).child("basariDurumu").setValue(1);
        });
        noBtn.setOnClickListener(view -> {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAMALAR);
            ref.child(id).child("basariDurumu").setValue(-1);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAMALAR);
        ref.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Tohumlama t = dataSnapshot.getValue(Tohumlama.class);

                if (t != null) {
                    t.setId(id);
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVANLAR);
                    ref.child(t.getHayvanID()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Hayvan h = dataSnapshot.getValue(Hayvan.class);

                            if (h != null) {
                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAR);
                                ref.child(t.getAtilanTohumID()).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        Tohum to = dataSnapshot.getValue(Tohum.class);

                                        if (to != null) {
                                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVAN_SAHIPLERI);
                                            ref.child(h.hayvanSahibiID).addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    HayvanSahibi hs = dataSnapshot.getValue(HayvanSahibi.class);

                                                    if (hs != null) {
                                                        hs.uid = dataSnapshot.getKey();
                                                        fill(t, h, to, hs);
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void fill(Tohumlama t, Hayvan h, Tohum to, HayvanSahibi hs) {
        HS.setText(hs.isim);
        hayvan.setText(h.hayvanBilgisi);
        tarih.setText(t.getTohumlamaTarihi());
        tohum.setText(to.isim);

        if (t.getBasariDurumu() == 0) {
            basariDurumuContainer.setVisibility(View.GONE);
            basariliOlduMu.setVisibility(View.VISIBLE);
        }else {
            basariDurumuContainer.setVisibility(View.VISIBLE);
            basariliOlduMu.setVisibility(View.GONE);

            if (t.getBasariDurumu() == 1)
                basariDurumu.setImageResource(R.drawable.ic_check);
            else
                basariDurumu.setImageResource(R.drawable.ic_close);
        }

        editBtn.setOnClickListener(view -> {
            Intent i = new Intent(this, YeniTohumlamaActivity.class);

            ArrayList<String> list = new ArrayList<>();
            list.add(Utils.UT_TOHUMLAMA_ID, t.getId());
            list.add(Utils.UT_HAYVAN_SAHIBI_ID, hs.uid);
            list.add(Utils.UT_HAYVAN_ID, t.getHayvanID());
            list.add(Utils.UT_TOHUM_ID, t.getAtilanTohumID());
            list.add(Utils.UT_TOHUMLAMA_TARIHI, t.getTohumlamaTarihi());
            list.add(Utils.UT_BASARI_DURUMU, String.valueOf(t.getBasariDurumu()));

            i.putExtra(Utils.EXTRA_MODE, Utils.MODE_UPDATE);
            i.putExtra(Utils.EXTRA_UT, list);
            startActivity(i);
        });
    }

    private void delete() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.tohumlama))
                .setMessage(getString(R.string.sure_delete_tohumlama))
                .setPositiveButton(getString(R.string.yes), (dialogInterface, i) -> {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAMALAR);
                    ref.child(id).removeValue();
                    finish();
                })
                .setNegativeButton(getString(R.string.no), null);
        dialog.show();
    }

}