package com.osmanyasirinan.sunitohumlama.activities.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.VeterinerHekim;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

public class UpdateProfileActivity extends AppCompatActivity {
    
    private EditText name, email, password;
    private AutoCompleteTextView province;
    private Button save;
    private ImageView back;
    
    private FirebaseAuth mAuth;
    
    private VeterinerHekim vh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
    
        name = findViewById(R.id.updateProfileName);
        email = findViewById(R.id.updateProfileEmail);
        password = findViewById(R.id.updateProfilePassword);
        province = findViewById(R.id.updateProfileProvince);
        
        back = findViewById(R.id.backInUpdateProfile);
        save = findViewById(R.id.updateProfileBtn);
    
        back.setOnClickListener(view -> finish());
        province.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, Utils.SEHIRLER));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                updateUI();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        name.addTextChangedListener(watcher);
        province.addTextChangedListener(watcher);

        fill();
    }

    private void fill() {
        if (mAuth.getCurrentUser() != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_UYELER);
            ref.child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    VeterinerHekim vt = snapshot.getValue(VeterinerHekim.class);

                    if (vt != null) {
                        name.setText(vt.name);
                        email.setText(vt.email);
                        province.setText(vt.province);

                        name.addTextChangedListener(Utils.watcher(name, Utils.NAME, vt.name));
                        province.addTextChangedListener(Utils.watcher(province, Utils.PROVINCE, vt.province));
                        email.addTextChangedListener(Utils.watcher(email, Utils.EMAIL, vt.email));

                        password.setOnClickListener(view -> changePassword());
                        UpdateProfileActivity.this.vh = vt;
                        save.setOnClickListener(view -> {
                            String errorMessage = checkInputs();
                            if (errorMessage == null) {
                                updateProvince();
                                if (save())
                                    Toast.makeText(UpdateProfileActivity.this, getString(R.string.bilgileriniz_guncellendi), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(UpdateProfileActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                            }else
                                Toast.makeText(UpdateProfileActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {}
            });
        }else finish();
    }

    private boolean save() {
        if (mAuth.getCurrentUser() != null) {
            mAuth.getCurrentUser().updateEmail(email.getText().toString().trim());
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_UYELER);
            return ref.child(mAuth.getCurrentUser().getUid()).setValue(new VeterinerHekim(
                    name.getText().toString().trim(),
                    email.getText().toString().trim(),
                    province.getText().toString().trim()
            )).isSuccessful();
        }else return false;
    }
    
    private String checkInputs() {
        if (Utils.checkInput(Utils.NAME, name.getText().toString().trim()))
            if (Utils.checkInput(Utils.EMAIL, email.getText().toString().trim()))
                if (Utils.checkInput(Utils.PROVINCE, province.getText().toString()))
                    return null;
                else
                    return Utils.getErrorMessage(Utils.PROVINCE, this);
            else
                return Utils.getErrorMessage(Utils.EMAIL, this);
        else
            return Utils.getErrorMessage(Utils.NAME, this);
    }

    private void updateProvince() {
        if (mAuth.getCurrentUser() != null) {
            Utils.updateUserProvince(this, province.getText().toString());
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_UYELER);
            ref.child(mAuth.getCurrentUser().getUid()).child(Utils.PROVINCE).setValue(province.getText().toString());
        }
    }
    
    private void updateUI() {
        if (!name.getText().toString().equals(vh.name) || !province.getText().toString().equals(vh.province) || !email.getText().toString().equals(vh.email))
            save.setVisibility(View.VISIBLE);
        else
            save.setVisibility(View.INVISIBLE);
    }
  
    private void changePassword() {
        if (mAuth.getCurrentUser() != null && mAuth.getCurrentUser().getEmail() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setMessage(R.string.sure_change_password)
                    .setPositiveButton(R.string.yes, ((dialogInterface, i) -> mAuth.sendPasswordResetEmail(mAuth.getCurrentUser().getEmail())))
                    .setNegativeButton(R.string.no, null);
            builder.show();
        }
    }

}