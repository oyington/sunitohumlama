package com.osmanyasirinan.sunitohumlama.activities.hayvansahibi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HayvanSahibiEditActivity extends AppCompatActivity {
    
    private String uid;
    private String province;

    private ImageView back;
    private EditText name, mahallekoy;
    private Spinner townSp;
    private Button button;

    private List<String> ilceler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hayvan_sahibi_edit);

        province = Utils.getUserProvince(this);
        
        uid = getIntent().getStringExtra(Utils.EXTRA_HSUID);
        
        back = findViewById(R.id.backInEditHS);
        name = findViewById(R.id.HSEditNameET);
        mahallekoy = findViewById(R.id.HSEditMahallekoyET);
        townSp = findViewById(R.id.HSEditTownSp);
        
        back.setOnClickListener(view -> finish());

        try {
            JSONObject towns = Utils.getTowns(HayvanSahibiEditActivity.this);
            if (towns != null && province != null) {
                JSONArray jsonArray = (JSONArray) towns.get(province);
                List<String> list = new ArrayList<>();
                list.add(getString(R.string.town));
                for (int i = 0; i < jsonArray.length(); i++)
                    list.add(jsonArray.get(i).toString());
                ilceler = list;

                ArrayAdapter<String> adapter = new ArrayAdapter<>(HayvanSahibiEditActivity.this, R.layout.spinner, ilceler);
                townSp.setAdapter(adapter);
                townSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (i == 0)
                            townSp.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.town_spinner, null));
                        else
                            townSp.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.town_spinner_focused, null));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        name.addTextChangedListener(Utils.watcher(name, Utils.HS_NAME));
        mahallekoy.addTextChangedListener(Utils.watcher(mahallekoy, Utils.MAHALLEKOY));

        button = findViewById(R.id.HSEditBtn);
        button.setOnClickListener(view -> update());
    }

    @Override
    protected void onStart() {
        super.onStart();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVAN_SAHIPLERI);
        ref.child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HayvanSahibi hs = dataSnapshot.getValue(HayvanSahibi.class);
                
                if (hs != null)
                    fill(hs);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    
    private void fill(HayvanSahibi hs) {
        name.setText(hs.isim);
        townSp.setSelection(ilceler.indexOf(hs.town));
        mahallekoy.setText(hs.mahallekoy);
    }

    private void update() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        String errorMessage = checkInputs();

        if (errorMessage == null){
            if (user != null) {
                HayvanSahibi hs = new HayvanSahibi(
                        user.getUid(),
                        name.getText().toString().trim(),
                        province,
                        townSp.getSelectedItem().toString().trim(),
                        mahallekoy.getText().toString()
                );

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVAN_SAHIPLERI);
                ref.child(uid).setValue(hs);
                finish();
            }
        }else Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    private String checkInputs() {
        Context c = this;

        if (Utils.checkInput(Utils.HS_NAME, name.getText().toString().trim()))
            if (townSp.getSelectedItemPosition() != 0)
                if (Utils.checkInput(Utils.MAHALLEKOY, mahallekoy.getText().toString().trim()))
                    return null;
                else
                    return Utils.getErrorMessage(Utils.MAHALLEKOY, c);
            else
                return Utils.getErrorMessage(Utils.TOWN, c);
        else
            return Utils.getErrorMessage(Utils.HS_NAME, c);
    }

}