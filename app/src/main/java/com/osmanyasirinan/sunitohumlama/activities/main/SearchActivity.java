package com.osmanyasirinan.sunitohumlama.activities.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.widget.ImageView;

import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.fragments.search.SearchHSFragment;
import com.osmanyasirinan.sunitohumlama.fragments.search.SearchHayvanFragment;
import com.osmanyasirinan.sunitohumlama.fragments.search.SearchTohumlamaFragment;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

public class SearchActivity extends AppCompatActivity {

    private int search;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        search = getIntent().getIntExtra(Utils.EXTRA_SEARCH, Utils.SEARCH_HS);
        updateUI();

        back = findViewById(R.id.backInSearch);
        back.setOnClickListener(view -> finish());
    }

    private void updateUI() {
        if (search == Utils.SEARCH_HS) {
            selectFragment(new SearchHSFragment());
        }else if (search == Utils.SEARCH_HAYVAN) {
            selectFragment(new SearchHayvanFragment());
        }else if (search == Utils.SEARCH_TOHUMLAMA) {
            selectFragment(new SearchTohumlamaFragment());
        }
    }

    private void selectFragment(Fragment f) {
        getSupportFragmentManager().beginTransaction().replace(R.id.searchLayout, f).commit();
    }

}