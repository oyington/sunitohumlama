package com.osmanyasirinan.sunitohumlama.auth;

import android.os.Bundle;

public interface StepListener {

    void onMove(int currentStep, Bundle inputs);
    void onBack(int currentStep, Bundle inputs);

}
