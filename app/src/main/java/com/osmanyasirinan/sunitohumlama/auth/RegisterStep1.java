package com.osmanyasirinan.sunitohumlama.auth;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

public class RegisterStep1 extends Fragment {

    private StepListener listener;

    // INPUTS
    private String NAME;
    private String EMAIL;
    private String PROVINCE;

    public RegisterStep1() {}

    public static RegisterStep1 newInstance(Bundle inputs) {
        RegisterStep1 fragment = new RegisterStep1();
        fragment.setArguments(inputs);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            NAME = getArguments().getString(Utils.NAME);
            EMAIL = getArguments().getString(Utils.EMAIL);
            PROVINCE = getArguments().getString(Utils.PROVINCE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_step1, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof StepListener)
            listener = (StepListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // INPUTLAR
        EditText nameET = view.findViewById(R.id.nameET);
        EditText emailET = view.findViewById(R.id.emailET);
        AutoCompleteTextView provinceACTV = view.findViewById(R.id.provinceACTV);

        // INPUTLARIN LISTENERLARI
        nameET.addTextChangedListener(Utils.watcher(nameET, Utils.NAME));
        emailET.addTextChangedListener(Utils.watcher(emailET, Utils.EMAIL));
        provinceACTV.addTextChangedListener(Utils.watcher(provinceACTV, Utils.PROVINCE));

        nameET.setOnFocusChangeListener(Utils.focusChangeListener(nameET, Utils.NAME));
        emailET.setOnFocusChangeListener(Utils.focusChangeListener(emailET, Utils.EMAIL));
        provinceACTV.setOnFocusChangeListener(Utils.focusChangeListener(provinceACTV, Utils.PROVINCE));

        // İleriki bir adımdan geri dönüldüğünde inputların doldurulması.
        if (NAME != null && EMAIL != null && PROVINCE != null) {
            nameET.setText(NAME);
            emailET.setText(EMAIL);
            provinceACTV.setText(PROVINCE);
        }

        // ACTV
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, Utils.SEHIRLER);
        provinceACTV.setAdapter(adapter);

        // DEVAM ET BUTONU
        Button move = view.findViewById(R.id.moveBtn1);
        move.setOnClickListener(view1 -> {
            Bundle inputs = new Bundle();

            inputs.putString(Utils.NAME, nameET.getText().toString().trim());
            inputs.putString(Utils.EMAIL, emailET.getText().toString().trim());
            inputs.putString(Utils.PROVINCE, provinceACTV.getText().toString().trim());

            if (getArguments() != null) {
                inputs.putString(Utils.ACCOUNT_TYPE, getArguments().getString(Utils.ACCOUNT_TYPE));
                inputs.putString(Utils.ORGANISATION, getArguments().getString(Utils.ORGANISATION));
                inputs.putString(Utils.TOWN, getArguments().getString(Utils.TOWN));
                inputs.putString(Utils.MAHALLEKOY, getArguments().getString(Utils.MAHALLEKOY));
            }

            String errorMessage = checkInputs(inputs);

            if (errorMessage == null)
                listener.onMove(1, inputs);
            else
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
        });

        // GERİ BUTONU
        ImageView back = view.findViewById(R.id.backBtn1);
        back.setOnClickListener(view1 -> listener.onBack(1, null));
    }

    private String checkInputs(Bundle inputs) {
        Context c = getActivity();

        if (Utils.checkInput(Utils.NAME, inputs.getString(Utils.NAME)))
            if (Utils.checkInput(Utils.EMAIL, inputs.getString(Utils.EMAIL)))
                if (Utils.checkInput(Utils.PROVINCE, inputs.getString(Utils.PROVINCE)))
                    return null;
                else
                    return Utils.getErrorMessage(Utils.PROVINCE, c);
            else
                return Utils.getErrorMessage(Utils.EMAIL, c);
        else
            return Utils.getErrorMessage(Utils.NAME, c);
    }

}