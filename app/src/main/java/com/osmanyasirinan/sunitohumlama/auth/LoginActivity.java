package com.osmanyasirinan.sunitohumlama.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.activities.main.MainActivity;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    private Button login;
    private TextView kayitTv, resetTv;
    private EditText email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.emailInput);
        email.addTextChangedListener(Utils.watcher(email, Utils.EMAIL));

        password = findViewById(R.id.passwordInput);
        password.addTextChangedListener(Utils.watcher(password, Utils.PASSWORD));

        kayitTv = findViewById(R.id.signupTv);
        kayitTv.setOnClickListener(view -> {
            Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(i);
        });

        resetTv = findViewById(R.id.resetPasswordTv);
        resetTv.setOnClickListener(view -> {
            if (Utils.checkInput(Utils.EMAIL, email.getText().toString()))
                new ResetDialog(getSupportFragmentManager(), email.getText().toString()).showDialog();
            else
                new ResetDialog(getSupportFragmentManager()).showDialog();
        });

        login = findViewById(R.id.loginButton);
        login.setOnClickListener(view -> {
            String e = email.getText().toString(), p = password.getText().toString();

            if (Utils.checkInput(Utils.PASSWORD, p) && Utils.checkInput(Utils.EMAIL, e))
                login(e, p);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) leave();
    }

    private void login(String email, String password){
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.info_please_wait));
        dialog.show();

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            dialog.dismiss();
            if (task.isSuccessful()) leave();
            else Toast.makeText(this, Utils.getAuthErrorMessage(task.getException(), this), Toast.LENGTH_SHORT).show();
        });
    }

    private void leave() {
        String uid = null;

        if (mAuth.getCurrentUser() != null)
            uid = mAuth.getCurrentUser().getUid();

        if (uid != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_UYELER);
            ref.child(uid).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String province = dataSnapshot.child(Utils.PROVINCE).getValue(String.class);
                    Utils.updateUserProvince(LoginActivity.this, province);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

}