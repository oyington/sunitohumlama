package com.osmanyasirinan.sunitohumlama.auth;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.VeterinerHekim;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

public class RegisterStep2 extends Fragment {

    FirebaseAuth mAuth;
    private StepListener listener;

    public RegisterStep2() {}

    public static RegisterStep2 newInstance(Bundle inputs) {
        RegisterStep2 fragment = new RegisterStep2();
        fragment.setArguments(inputs);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_step2, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof StepListener)
            listener = (StepListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAuth = FirebaseAuth.getInstance();

        // INPUTLAR
        EditText pass = view.findViewById(R.id.passwordET), pass2 = view.findViewById(R.id.password2ET);

        pass.addTextChangedListener(Utils.watcher(pass, Utils.PASSWORD));
        pass.setOnFocusChangeListener(Utils.focusChangeListener(pass, Utils.PASSWORD));

        pass2.addTextChangedListener(Utils.watcher(pass2, Utils.PASSWORD2, pass.getText().toString()));

        // İLERİ BUTONU
        Button move = view.findViewById(R.id.moveBtn3);
        move.setOnClickListener(view1 -> {
            Context c = getActivity();

            String password = pass.getText().toString(), password2 = pass2.getText().toString();

            if (Utils.checkInput(Utils.PASSWORD, password)) {
                if (password.equals(password2)){
                    if (getArguments() != null) {
                        Bundle args = getArguments();
                        VeterinerHekim vh = new VeterinerHekim(
                                args.getString(Utils.NAME),
                                args.getString(Utils.EMAIL),
                                args.getString(Utils.PROVINCE)
                        );
                        register(vh, password);
                    }
                }else
                    Toast.makeText(c, Utils.getErrorMessage(Utils.PASSWORD2, c), Toast.LENGTH_SHORT).show();
            }else
                Toast.makeText(c, Utils.getErrorMessage(Utils.PASSWORD, c), Toast.LENGTH_SHORT).show();
        });

        // GERİ BUTONU
        ImageView back = view.findViewById(R.id.backBtn3);
        back.setOnClickListener(view1 -> listener.onBack(2, getArguments()));
    }

    private void register(VeterinerHekim vh, String password) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.info_please_wait));
        dialog.show();

        mAuth.createUserWithEmailAndPassword(vh.email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                FirebaseUser user = mAuth.getCurrentUser();

                if (user != null) {
                    FirebaseDatabase db = FirebaseDatabase.getInstance();
                    DatabaseReference ref = db.getReference();

                    ref.child(Utils.TABLO_UYELER).child(user.getUid()).setValue(vh);

                    dialog.dismiss();

                    if (getActivity() != null)
                        Utils.updateUserProvince(getActivity(), vh.province);

                    Toast.makeText(getActivity(), getString(R.string.info_register_success), Toast.LENGTH_SHORT).show();
                    listener.onMove(2, null);
                }
            }else {
                dialog.dismiss();
                Toast.makeText(getActivity(), Utils.getAuthErrorMessage(task.getException(), getActivity()), Toast.LENGTH_SHORT).show();
            }
        });
    }

}