package com.osmanyasirinan.sunitohumlama.auth;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.osmanyasirinan.sunitohumlama.R;

public class RegisterActivity extends AppCompatActivity implements StepListener {

    private int currentStep;
    private Bundle currentInputs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        FrameLayout container = findViewById(R.id.registerContainer);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportFragmentManager().beginTransaction().replace(R.id.registerContainer, new RegisterStep1()).commit();
        currentStep = 1;
    }

    private void replaceFragment(int step, boolean forward, Bundle inputs) {
        if (step == 0 || step == 3){
            finish();
        }else {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            if (forward)
                transaction.setCustomAnimations(
                        R.anim.enter_right_to_left, R.anim.exit_right_to_left,
                        R.anim.enter_left_to_right, R.anim.exit_left_to_right);
            else
                transaction.setCustomAnimations(
                        R.anim.enter_left_to_right, R.anim.exit_left_to_right,
                        R.anim.enter_right_to_left, R.anim.exit_right_to_left
                );

            Fragment f;

            if (inputs == null) {
                if (step == 1)
                    f = new RegisterStep1();
                else
                    f = new RegisterStep2();
            }else {
                if (step == 1)
                    f = RegisterStep1.newInstance(inputs);
                else
                    f = RegisterStep2.newInstance(inputs);
            }

            transaction.replace(R.id.registerContainer, f).commit();
            currentStep = step;
            currentInputs = inputs;
        }
    }

    @Override
    public void onMove(int currentStep, Bundle inputs) {
        replaceFragment(currentStep + 1, true, inputs);
    }

    @Override
    public void onBack(int currentStep, Bundle inputs) {
        replaceFragment(currentStep - 1, false, inputs);
    }

    @Override
    public void onBackPressed() {
        onBack(currentStep, currentInputs);
    }

}