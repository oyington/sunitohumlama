package com.osmanyasirinan.sunitohumlama.auth;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.FragmentManager;

import com.google.firebase.auth.FirebaseAuth;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

public class ResetDialog extends AppCompatDialogFragment {

    public static final String TAG = "ResetDialog";

    private FirebaseAuth mAuth;
    private EditText email;
    private FragmentManager manager;
    private String autoFill;
    private Button reset;

    public ResetDialog(FragmentManager manager) {
        this.manager = manager;
    }

    public ResetDialog(FragmentManager manager, String autoFill) {
        this.manager = manager;
        this.autoFill = autoFill;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.dialog_reset, null);
        builder.setView(view);

        mAuth = FirebaseAuth.getInstance();

        email = view.findViewById(R.id.resetEmail);
        email.addTextChangedListener(Utils.watcher(email, Utils.EMAIL));
        if (autoFill != null)
            email.setText(autoFill);

        reset = view.findViewById(R.id.resetBtn);
        reset.setOnClickListener(view1 -> {
            if (Utils.checkInput(Utils.EMAIL, email.getText().toString()))
                mAuth.sendPasswordResetEmail(email.getText().toString()).addOnCompleteListener(task -> leave(task.isSuccessful()));
        });

        return builder.create();
    }

    public void showDialog() {
        show(manager, TAG);
    }

    private void leave(boolean successful) {
        Context c = getActivity();

        if (c != null) {
            if (successful)
                Toast.makeText(c, c.getString(R.string.info_sent), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(c, c.getString(R.string.error_account_does_not_exist), Toast.LENGTH_SHORT).show();
        }

        dismiss();
    }

}