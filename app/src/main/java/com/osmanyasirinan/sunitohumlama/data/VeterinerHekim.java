package com.osmanyasirinan.sunitohumlama.data;

import androidx.annotation.NonNull;

public class VeterinerHekim {

    public String uid;

    public String name;

    public String email;

    public String province;

    public VeterinerHekim() {}

    public VeterinerHekim(String name, String email, String province) {
        this.name = name;
        this.email = email;
        this.province = province;
        this.uid = "";
    }

}