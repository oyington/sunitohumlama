package com.osmanyasirinan.sunitohumlama.data;

public class Hayvan {

    public String hayvanBilgisi;

    public String uyeID;

    public String hayvanSahibiID;

    public String id;

    public Hayvan() {}

    public Hayvan(String hayvanBilgisi, String uyeID, String hayvanSahibiID) {
        this.hayvanBilgisi = hayvanBilgisi;
        this.uyeID = uyeID;
        this.hayvanSahibiID = hayvanSahibiID;
    }

}
