package com.osmanyasirinan.sunitohumlama.data;

public class HayvanSahibi {

    public String uyeID;

    public String isim;

    public String province;

    public String town;

    public String mahallekoy;

    public String uid;

    public HayvanSahibi() {}

    public HayvanSahibi(String uyeID, String isim, String province, String town, String mahallekoy) {
        this.uyeID = uyeID;
        this.isim = isim;
        this.province = province;
        this.town = town;
        this.mahallekoy = mahallekoy;
    }

    public HayvanSahibi(String uyeID, String isim, String province, String town, String mahallekoy, String uid) {
        this.uyeID = uyeID;
        this.isim = isim;
        this.province = province;
        this.town = town;
        this.mahallekoy = mahallekoy;
        this.uid = uid;
    }
}
