package com.osmanyasirinan.sunitohumlama.data;

public class Tohumlama {

    private String uyeID;

    private String hayvanID;

    private String tohumlamaTarihi;

    private String atilanTohumID;

    private int basariDurumu;

    private String id;

    private String hayvanSahibiID;

    public Tohumlama() {}

    public Tohumlama(String uyeID, String hayvanID, String tohumlamaTarihi, String atilanTohumID, int basariDurumu, String hayvanSahibiID) {
        this.uyeID = uyeID;
        this.hayvanID = hayvanID;
        this.tohumlamaTarihi = tohumlamaTarihi;
        this.atilanTohumID = atilanTohumID;
        this.basariDurumu = basariDurumu;
        this.hayvanSahibiID = hayvanSahibiID;
    }

    public String getHayvanSahibiID() {
        return hayvanSahibiID;
    }

    public void setHayvanSahibiID(String hayvanSahibiID) {
        this.hayvanSahibiID = hayvanSahibiID;
    }

    public String getHayvanID() {
        return hayvanID;
    }

    public void setHayvanID(String hayvanID) {
        this.hayvanID = hayvanID;
    }

    public String getTohumlamaTarihi() {
        return tohumlamaTarihi;
    }

    public void setTohumlamaTarihi(String tohumlamaTarihi) {
        this.tohumlamaTarihi = tohumlamaTarihi;
    }

    public String getAtilanTohumID() {
        return atilanTohumID;
    }

    public void setAtilanTohumID(String atilanTohumID) {
        this.atilanTohumID = atilanTohumID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getBasariDurumu() {
        return basariDurumu;
    }

    public void setBasariDurumu(int basariDurumu) {
        this.basariDurumu = basariDurumu;
    }

    public String getUyeID() {
        return uyeID;
    }

    public void setUyeID(String uyeID) {
        this.uyeID = uyeID;
    }
}
