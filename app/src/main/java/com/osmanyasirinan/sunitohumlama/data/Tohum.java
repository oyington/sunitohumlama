package com.osmanyasirinan.sunitohumlama.data;

public class Tohum {

    public String isim;

    public int sayi;

    public String uyeID;

    public String id;

    public Tohum() {}

    public Tohum(String isim, int sayi, String uyeID) {
        this.isim = isim;
        this.sayi = sayi;
        this.uyeID = uyeID;
    }

}
