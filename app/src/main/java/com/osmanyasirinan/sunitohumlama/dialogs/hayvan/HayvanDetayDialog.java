package com.osmanyasirinan.sunitohumlama.dialogs.hayvan;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.FragmentManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.activities.tohumlama.YeniTohumlamaActivity;
import com.osmanyasirinan.sunitohumlama.data.Hayvan;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import java.util.ArrayList;

public class HayvanDetayDialog extends AppCompatDialogFragment {

    public static final String TAG = "HayvanDetayDialog";

    private FirebaseAuth mAuth;

    private FragmentManager manager;
    private Hayvan hayvan;

    private EditText hayvanBilgisi, hayvanSahibi;
    private ImageView edit, delete, save, back, add;

    private boolean inEditMode;

    public HayvanDetayDialog(FragmentManager manager, Hayvan hayvan) {
        this.manager = manager;
        this.hayvan = hayvan;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = View.inflate(getActivity(), R.layout.dialog_hayvan_detay, null);
        builder.setView(v);

        hayvanSahibi = v.findViewById(R.id.hayvanDetayHS);
        hayvanBilgisi = v.findViewById(R.id.hayvanDetayHayvanBilgisi);

        edit = v.findViewById(R.id.hayvanDetayEditBtn);
        delete = v.findViewById(R.id.hayvanDetayDeleteBtn);
        save = v.findViewById(R.id.hayvanDetaySaveBtn);
        back = v.findViewById(R.id.backInHayvanDetay);

        add = v.findViewById(R.id.hayvanDetayYeniTohumlamaBtn);

        inEditMode = false;

        fill();
        updateUI(inEditMode);

        edit.setOnClickListener(view -> updateUI(true));
        delete.setOnClickListener(view -> createSureDialog().show());
        save.setOnClickListener(view -> update());
        back.setOnClickListener(view -> {
            if (inEditMode) {
                updateUI(false);
                hayvanBilgisi.setText(hayvan.hayvanBilgisi);
            }else dismiss();
        });
        add.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), YeniTohumlamaActivity.class);

            ArrayList<String> list = new ArrayList<>();
            list.add(Utils.SF_HAYVAN_SAHIBI_ID, hayvan.hayvanSahibiID);
            list.add(Utils.SF_HAYVAN_ID, hayvan.id);

            i.putExtra(Utils.EXTRA_MODE, Utils.MODE_SPECIFIED_FIELDS);
            i.putExtra(Utils.EXTRA_SPECIFIED_FIELDS, list);
            startActivity(i);
        });

        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();
    }

    private void fill() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVANLAR);
        ref.child(hayvan.id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Hayvan h = dataSnapshot.getValue(Hayvan.class);

                if (h != null)
                    hayvanBilgisi.setText(h.hayvanBilgisi);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        DatabaseReference ref2 = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVAN_SAHIPLERI);
        ref2.child(hayvan.hayvanSahibiID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HayvanSahibi hs = dataSnapshot.getValue(HayvanSahibi.class);

                if (hs != null)
                    hayvanSahibi.setText(hs.isim);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateUI(boolean inEditMode) {
        this.inEditMode = inEditMode;

        if (inEditMode) {
            hayvanBilgisi.setEnabled(true);

            edit.setVisibility(View.GONE);
            delete.setVisibility(View.GONE);
            save.setVisibility(View.VISIBLE);
        }else {
            hayvanBilgisi.setEnabled(false);

            edit.setVisibility(View.VISIBLE);
            delete.setVisibility(View.VISIBLE);
            save.setVisibility(View.GONE);
        }
    }

    private void delete() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVANLAR);
        ref.child(hayvan.id).removeValue();
        dismiss();
    }

    private void update() {
        if (mAuth.getCurrentUser() != null && getActivity() != null) {
            if (!hayvanBilgisi.getText().toString().trim().isEmpty()) {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVANLAR);
                Hayvan yeni = new Hayvan(
                        hayvanBilgisi.getText().toString().trim(),
                        hayvan.hayvanSahibiID,
                        mAuth.getCurrentUser().getUid()
                );
                ref.child(hayvan.id).setValue(yeni);
                hayvan = yeni;
                dismiss();
            }else Toast.makeText(getActivity(), Utils.getErrorMessage(Utils.HAYVAN_BILGISI, getActivity()), Toast.LENGTH_SHORT).show();
        }
    }

    private AlertDialog.Builder createSureDialog() {
        return new AlertDialog.Builder(getActivity())
                .setTitle(hayvanSahibi.getText().toString())
                .setMessage(getString(R.string.sure_delete_hayvan))
                .setPositiveButton(getString(R.string.yes), (dialogInterface, i) -> delete())
                .setNegativeButton(getString(R.string.no), null);
    }

    public void createDialog() { show(manager, TAG); }

}
