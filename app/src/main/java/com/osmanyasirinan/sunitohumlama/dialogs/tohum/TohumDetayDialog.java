package com.osmanyasirinan.sunitohumlama.dialogs.tohum;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.FragmentManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.Tohum;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;
import androidx.appcompat.app.AlertDialog.Builder;

public class TohumDetayDialog extends AppCompatDialogFragment {

    public static final String TAG = "TohumDetayDialog";

    private FirebaseAuth mAuth;

    private FragmentManager manager;
    private Tohum tohum;

    private EditText adi, sayisi;
    private ImageView edit, delete, save, back;

    private boolean inEditMode;

    public TohumDetayDialog(FragmentManager manager, Tohum tohum) {
        this.manager = manager;
        this.tohum = tohum;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.dialog_tohum_detay, null);
        builder.setView(view);

        adi = view.findViewById(R.id.tohumDetayAdi);
        sayisi = view.findViewById(R.id.tohumDetaySayisi);

        edit = view.findViewById(R.id.tohumDetayEditBtn);
        delete = view.findViewById(R.id.tohumDetayDeleteBtn);
        save = view.findViewById(R.id.tohumDetaySaveBtn);
        back = view.findViewById(R.id.backInTohumDetay);

        inEditMode = false;

        fill();
        updateUI(inEditMode);

        edit.setOnClickListener(view1 -> updateUI(true));
        delete.setOnClickListener(view1 -> createSureDialog().show());
        save.setOnClickListener(view1 -> update());
        back.setOnClickListener(view1 -> {
            if (inEditMode) {
                updateUI(false);
                sayisi.setText(tohum.sayi);
            }else dismiss();
        });

        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();

        mAuth = FirebaseAuth.getInstance();
    }

    public void showDialog() { show(manager, TAG); }

    private void fill() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAR);
        ref.child(tohum.id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Tohum t = dataSnapshot.getValue(Tohum.class);

                if (t != null) {
                    adi.setText(t.isim);
                    sayisi.setText(String.valueOf(t.sayi));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateUI(boolean inEditMode) {
        this.inEditMode = inEditMode;

        if (inEditMode) {
            adi.setEnabled(true);
            sayisi.setEnabled(true);

            edit.setVisibility(View.GONE);
            delete.setVisibility(View.GONE);
            save.setVisibility(View.VISIBLE);
        }else {
            adi.setEnabled(false);
            sayisi.setEnabled(false);

            edit.setVisibility(View.VISIBLE);
            delete.setVisibility(View.VISIBLE);
            save.setVisibility(View.GONE);
        }
    }

    private void delete() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAR);
        ref.child(tohum.id).removeValue();
        dismiss();
    }

    private void update() {
        Context c = getActivity();

        if (mAuth.getCurrentUser() != null) {
            if (Utils.checkInput(Utils.TOHUM_ADI, adi.getText().toString().trim()))
                if (!sayisi.getText().toString().isEmpty()) {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAR);
                    Tohum yeni = new Tohum(
                            adi.getText().toString().trim(),
                            Integer.parseInt(sayisi.getText().toString()),
                            mAuth.getCurrentUser().getUid()
                    );
                    ref.child(tohum.id).setValue(yeni);
                    tohum = yeni;
                    dismiss();
                }
                else
                    Toast.makeText(c, Utils.getErrorMessage(Utils.TOHUM_SAYISI, c), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(c, Utils.getErrorMessage(Utils.TOHUM_ADI, c), Toast.LENGTH_SHORT).show();
        }
    }

    private AlertDialog.Builder createSureDialog() {
        return new AlertDialog.Builder(getActivity())
                .setTitle(adi.getText().toString())
                .setMessage(getString(R.string.sure_delete_tohum))
                .setPositiveButton(getString(R.string.yes), (dialogInterface, i) -> delete())
                .setNegativeButton(getString(R.string.no), null);
    }

}
