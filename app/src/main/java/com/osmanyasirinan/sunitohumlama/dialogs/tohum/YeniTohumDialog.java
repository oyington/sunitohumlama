package com.osmanyasirinan.sunitohumlama.dialogs.tohum;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.FragmentManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.Tohum;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

public class YeniTohumDialog extends AppCompatDialogFragment {

    public static final String TAG = "YeniTohumDialog";

    private FragmentManager manager;
    private EditText tohumadi, sayisi;
    private Button button;
    private FirebaseAuth mAuth;

    public YeniTohumDialog(FragmentManager manager) { this.manager = manager; }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.dialog_yeni_tohum, null);
        builder.setView(view);

        mAuth = FirebaseAuth.getInstance();

        tohumadi = view.findViewById(R.id.yeniTohumET);
        sayisi = view.findViewById(R.id.yeniTohumSayiET);

        button = view.findViewById(R.id.yeniTohumBtn);

        tohumadi.addTextChangedListener(Utils.watcher(tohumadi, Utils.TOHUM_ADI));

        button.setOnClickListener(view1 -> submit());

        return builder.create();
    }

    public void showDialog() { show(manager, TAG); }

    private void submit() {
        Context c = getActivity();

        if (mAuth.getCurrentUser() != null) {
            if (Utils.checkInput(Utils.TOHUM_ADI, tohumadi.getText().toString().trim()))
                if (!sayisi.getText().toString().isEmpty())
                    insert(new Tohum(
                            tohumadi.getText().toString().trim(),
                            Integer.parseInt(sayisi.getText().toString()),
                            mAuth.getCurrentUser().getUid()
                    ));
                else
                    Toast.makeText(c, Utils.getErrorMessage(Utils.TOHUM_SAYISI, c), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(c, Utils.getErrorMessage(Utils.TOHUM_ADI, c), Toast.LENGTH_SHORT).show();
        }
    }

    private void insert(Tohum t) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAR);
        ref.push().setValue(t);
        dismiss();
    }

}
