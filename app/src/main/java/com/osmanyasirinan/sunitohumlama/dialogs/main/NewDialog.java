package com.osmanyasirinan.sunitohumlama.dialogs.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.FragmentManager;

import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.activities.hayvansahibi.YeniHayvanSahibiActivity;
import com.osmanyasirinan.sunitohumlama.activities.tohumlama.YeniTohumlamaActivity;
import com.osmanyasirinan.sunitohumlama.dialogs.hayvan.YeniHayvanDialog;

public class NewDialog extends AppCompatDialogFragment {

    public static final String TAG = "NewDialog";
    private FragmentManager manager;
    private Button hs, hayvan, tohumlama;

    public NewDialog(FragmentManager manager) {
        this.manager = manager;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.dialog_new, null);
        builder.setView(view);

        hs = view.findViewById(R.id.newHayvanSahibiBtn);
        hayvan = view.findViewById(R.id.newHayvanBtn);
        tohumlama = view.findViewById(R.id.newTohumlamaBtn);

        hs.setOnClickListener(view1 -> hs());
        hayvan.setOnClickListener(view1 -> hayvan());
        tohumlama.setOnClickListener(view1 -> tohumlama());

        return builder.create();
    }

    public void createDialog() { show(manager, TAG); }

    private void hs() {
        Intent i = new Intent(getActivity(), YeniHayvanSahibiActivity.class);
        startActivity(i);
        dismiss();
    }

    private void hayvan() {
        if (getActivity() != null)
            new YeniHayvanDialog(getActivity().getSupportFragmentManager()).createDialog();
        dismiss();
    }

    private void tohumlama() {
        Intent i = new Intent(getActivity(), YeniTohumlamaActivity.class);
        startActivity(i);
        dismiss();
    }

}
