package com.osmanyasirinan.sunitohumlama.dialogs.hayvan;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.Hayvan;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;
import com.osmanyasirinan.sunitohumlama.dialogs.main.HSSelectDialog;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class YeniHayvanDialog extends AppCompatDialogFragment implements Utils.HSListener {

    public static final String TAG = "YeniHayvanDialog";

    private FragmentManager manager;
    private HayvanSahibi autoFillHS;

    private HayvanSahibi selectedHS;

    private EditText HSET;
    private EditText hayvanBilgisiET;

    private FirebaseAuth mAuth;

    public YeniHayvanDialog(FragmentManager manager) {
        this.manager = manager;
    }

    public YeniHayvanDialog(FragmentManager manager, HayvanSahibi autoFillHS) {
        this.manager = manager;
        this.autoFillHS = autoFillHS;
        selectedHS = autoFillHS;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = View.inflate(getActivity(), R.layout.dialog_yeni_hayvan, null);
        builder.setView(v);

        HSET = v.findViewById(R.id.yeniHayvanHS);
        hayvanBilgisiET = v.findViewById(R.id.yeniHayvanET);

        mAuth = FirebaseAuth.getInstance();

        List<HayvanSahibi> list = new ArrayList<>();

        if (mAuth.getCurrentUser() != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVAN_SAHIPLERI);
            Query query = ref.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        HayvanSahibi hs = ds.getValue(HayvanSahibi.class);

                        if (hs != null) {
                            hs.uid = ds.getKey();
                            list.add(hs);
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        HSET.setFocusable(false);
        HSET.setOnClickListener(view -> {
            if (getActivity() != null)
                new HSSelectDialog(getActivity().getSupportFragmentManager(), list, this).createDialog();
        });

        hayvanBilgisiET.addTextChangedListener(Utils.watcher(hayvanBilgisiET, Utils.HAYVAN_BILGISI));
        HSET.addTextChangedListener(Utils.watcher(HSET, Utils.HS_NAME));

        Button button = v.findViewById(R.id.yeniHayvanBtn);
        button.setOnClickListener(view -> leave());

        if (autoFillHS != null)
            HSET.setText(autoFillHS.isim);

        return builder.create();
    }

    public void createDialog() {
        show(manager, TAG);
    }

    private void leave() {
        if (getActivity() != null) {
            String errorMessage;

            if (selectedHS != null)
                if (!hayvanBilgisiET.getText().toString().trim().isEmpty())
                    errorMessage = null;
                else
                    errorMessage = Utils.getErrorMessage(Utils.HAYVAN_BILGISI, getActivity());
            else
                errorMessage = getActivity().getString(R.string.error_select_hs);

            if (errorMessage == null)
                submit();
            else Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
        }
    }

    private void submit() {
        if (mAuth.getCurrentUser() != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVANLAR);
            ref.push().setValue(new Hayvan(
                    hayvanBilgisiET.getText().toString().trim(),
                    mAuth.getCurrentUser().getUid(),
                    selectedHS.uid
            ));
            dismiss();
        }
    }

    @Override
    public void onHSSelected(HayvanSahibi selected) {
        selectedHS = selected;
        HSET.setText(selected.isim);
    }

}
