package com.osmanyasirinan.sunitohumlama.dialogs.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.FragmentManager;

import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.activities.main.SearchActivity;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

public class SearchDialog extends AppCompatDialogFragment {

    public static final String TAG = "SearchDialog";
    private FragmentManager manager;
    private Button HS, hayvan, tohumlama;

    public SearchDialog(FragmentManager manager) {
        this.manager = manager;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.dialog_search, null);
        builder.setView(view);

        HS = view.findViewById(R.id.searchDialogBtnHS);
        hayvan = view.findViewById(R.id.searchDialogBtnHayvan);
        tohumlama = view.findViewById(R.id.searchDialogBtnTohumlama);

        HS.setOnClickListener(view1 -> hs());
        hayvan.setOnClickListener(view1 -> hayvan());
        tohumlama.setOnClickListener(view1 -> tohumlama());

        return builder.create();
    }

    private void tohumlama() {
        Intent i = new Intent(getActivity(), SearchActivity.class);
        i.putExtra(Utils.EXTRA_SEARCH, Utils.SEARCH_TOHUMLAMA);
        startActivity(i);
        dismiss();
    }

    private void hayvan() {
        Intent i = new Intent(getActivity(), SearchActivity.class);
        i.putExtra(Utils.EXTRA_SEARCH, Utils.SEARCH_HAYVAN);
        startActivity(i);
        dismiss();
    }

    private void hs() {
        Intent i = new Intent(getActivity(), SearchActivity.class);
        i.putExtra(Utils.EXTRA_SEARCH, Utils.SEARCH_HS);
        startActivity(i);
        dismiss();
    }

    public void createDialog() {show(manager, TAG); }

}
