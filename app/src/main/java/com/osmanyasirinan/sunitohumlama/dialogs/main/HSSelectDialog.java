package com.osmanyasirinan.sunitohumlama.dialogs.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.FragmentManager;

import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;
import com.osmanyasirinan.sunitohumlama.utilities.HSSpinnerListAdapter;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class HSSelectDialog extends AppCompatDialogFragment {

    public static final String TAG = "HSSpinnerDialog";

    private Utils.HSListener listener;

    private FragmentManager manager;

    private List<HayvanSahibi> list;
    private List<HayvanSahibi> newList;
    private ListView listView;
    private SearchView search;

    private HSSpinnerListAdapter adapter;

    public HSSelectDialog(FragmentManager manager, List<HayvanSahibi> list, Utils.HSListener listener) { this.manager = manager; this.list = list; this.listener = listener; }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = View.inflate(getActivity(), R.layout.dialog_hs_select, null);
        builder.setView(v);

        listView = v.findViewById(R.id.HSSpinnerDialogLV);
        search = v.findViewById(R.id.HSSpinnerDialogSearch);

        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (getActivity() != null)
            adapter = new HSSpinnerListAdapter(getActivity(), list);

        listView.setAdapter(adapter);

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                String word = s.trim();

                if (!word.isEmpty()) {
                    newList = new ArrayList<>();

                    for (HayvanSahibi hs : list) {
                        if (hs.isim.toLowerCase().contains(s.toLowerCase()))
                            newList.add(hs);
                    }
                }else newList = null;

                if (getActivity() != null) {
                    if (newList == null)
                        adapter = new HSSpinnerListAdapter(getActivity(), list);
                    else
                        adapter = new HSSpinnerListAdapter(getActivity(), newList);
                }

                listView.setAdapter(adapter);

                return false;
            }
        });

        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            if (newList == null)
                listener.onHSSelected(list.get(i));
            else
                listener.onHSSelected(newList.get(i));
            dismiss();
        });
    }

    public void createDialog() { show(manager, TAG); }

}
