package com.osmanyasirinan.sunitohumlama.fragments.search;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.activities.main.FilteredListActivity;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;
import com.osmanyasirinan.sunitohumlama.data.Tohum;
import com.osmanyasirinan.sunitohumlama.dialogs.main.HSSelectDialog;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SearchTohumlamaFragment extends Fragment implements Utils.HSListener {

    public static final String BASLANGIC_TAG = "baslangic";
    public static final String BITIS_TAG = "bitis";

    private List<Tohum> tohumlar;
    private List<HayvanSahibi> hayvanSahipleri;

    private Button filterHS, filterTohum, filterTarih, filterBasariDurumu, submit, baslangicBtn, bitisBtn, filterBaslangic, filterBitis;
    private ImageView closeHS, closeTohum, closeBaslangic, closeBitis, closeBasariDurumu;
    private TextView baslangicTv, bitisTv;
    private Spinner tohumSp, basariDurumuSp;
    private LinearLayout HSContainer, tohumContainer, basariDurumuContainer, tarihContainer;
    private EditText HS;

    private boolean HSKey, tohumKey, tarihKey, baslangicKey, bitisKey, basariDurumuKey;
    private FirebaseAuth mAuth;

    private long selectedStartDate = 0, selectedEndDate = 0;
    private HayvanSahibi selectedHS;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_tohumlama, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        filterHS = view.findViewById(R.id.searchTohumlamaAddHSFilter);
        filterTohum = view.findViewById(R.id.searchTohumlamaAddTohumFilter);
        filterTarih = view.findViewById(R.id.searchTohumlamaAddTarihFilter);
        filterBasariDurumu = view.findViewById(R.id.searchTohumlamaAddBasariDurumuFilter);
        submit = view.findViewById(R.id.searchInTohumlama);
        baslangicBtn = view.findViewById(R.id.searchTohumlamaPickBaslangicBtn);
        bitisBtn = view.findViewById(R.id.searchTohumlamaPickBitisBtn);
        filterBaslangic = view.findViewById(R.id.searchTohumlamaAddBaslangicBtn);
        filterBitis = view.findViewById(R.id.searchTohumlamaAddBitisBtn);
        closeHS = view.findViewById(R.id.closeTohumlamaHS);
        closeTohum = view.findViewById(R.id.closeTohumlamaTohum);
        closeBaslangic = view.findViewById(R.id.closeTohumlamaBaslangic);
        closeBitis = view.findViewById(R.id.closeTohumlamaBitis);
        closeBasariDurumu = view.findViewById(R.id.closeTohumlamaBasariDurumu);
        baslangicTv = view.findViewById(R.id.searchTohumlamaBaslangicTv);
        bitisTv = view.findViewById(R.id.searchTohumlamaBitisTv);
        tohumSp = view.findViewById(R.id.searchTohumlamaTohum);
        basariDurumuSp = view.findViewById(R.id.searchTohumlamaBasariDurumu);
        HSContainer = view.findViewById(R.id.searchTohumlamaHSContainer);
        tohumContainer = view.findViewById(R.id.searchTohumlamaTohumContainer);
        tarihContainer = view.findViewById(R.id.searchTohumlamaTarihContainer);
        basariDurumuContainer = view.findViewById(R.id.searchTohumlamaBasariDurumuContainer);
        HS = view.findViewById(R.id.searchTohumlamaHS);

        HSKey = false;
        tohumKey = false;
        tarihKey = false;
        baslangicKey = false;
        bitisKey = false;
        basariDurumuKey = false;

        HS.addTextChangedListener(Utils.watcher(HS, Utils.HS_NAME));
        tohumSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    tohumSp.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.tohum_spinner, null));
                else
                    tohumSp.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.tohum_spinner_focused, null));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        filterHS.setOnClickListener(view1 -> updateUI(Utils.HS_NAME, true));
        closeHS.setOnClickListener(view1 -> updateUI(Utils.HS_NAME, false));
        filterTohum.setOnClickListener(view1 -> updateUI(Utils.TOHUM_ADI, true));
        closeTohum.setOnClickListener(view1 -> updateUI(Utils.TOHUM_ADI, false));
        filterTarih.setOnClickListener(view1 -> updateUI(Utils.TOHUMLAMA_TARIHI, true));
        filterBaslangic.setOnClickListener(view1 -> updateUI(Utils.BASLANGIC_TARIHI, true));
        closeBaslangic.setOnClickListener(view1 -> updateUI(Utils.BASLANGIC_TARIHI, false));
        filterBitis.setOnClickListener(view1 -> updateUI(Utils.BITIS_TARIHI, true));
        closeBitis.setOnClickListener(view1 -> updateUI(Utils.BITIS_TARIHI, false));
        filterBasariDurumu.setOnClickListener(view1 -> updateUI(Utils.BASARI_DURUMU, true));
        closeBasariDurumu.setOnClickListener(view1 -> updateUI(Utils.BASARI_DURUMU, false));
        submit.setOnClickListener(view1 -> submit());
    }

    private void submit() {
        ArrayList<String> list = new ArrayList<>();

        if (selectedHS != null)
            list.add(Utils.SEARCHF_HAYVAN_SAHIBI_ID, selectedHS.uid);
        else
            list.add(Utils.SEARCHF_HAYVAN_SAHIBI_ID, "");

        if (tohumSp.getSelectedItemPosition() != 0)
            list.add(Utils.SEARCHF_TOHUM_ID, tohumlar.get(tohumSp.getSelectedItemPosition() - 1).id);
        else
            list.add(Utils.SEARCHF_TOHUM_ID, "");

        if (selectedStartDate != 0)
            list.add(Utils.SEARCHF_BASLANGIC_MILLIS, String.valueOf(selectedStartDate));
        else
            list.add(Utils.SEARCHF_BASLANGIC_MILLIS, "");

        if (selectedEndDate != 0)
            list.add(Utils.SEARCHF_BITIS_MILLIS, String.valueOf(selectedEndDate));
        else
            list.add(Utils.SEARCHF_BITIS_MILLIS, "");

        if (basariDurumuKey)
            list.add(Utils.SEARCHF_BASARI_DURUMU, "" + basariDurumuSp.getSelectedItemPosition());
        else
            list.add(Utils.SEARCHF_BASARI_DURUMU, "");

        Intent i = new Intent(getActivity(), FilteredListActivity.class);
        i.putExtra(Utils.EXTRA_SEARCH, Utils.SEARCH_TOHUMLAMA);
        i.putExtra(Utils.EXTRA_SEARCH_FIELDS, list);
        startActivity(i);
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();

        fill();
    }

    private void fill() {
        if (mAuth.getCurrentUser() != null){
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAR);
            Query query = ref.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    List<String> list = new ArrayList<>();
                    tohumlar = new ArrayList<>();
                    list.add(getString(R.string.atilan_tohum));
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        Tohum t = ds.getValue(Tohum.class);

                        if (t != null) {
                            t.id = ds.getKey();
                            tohumlar.add(t);
                            list.add(t.isim);
                        }
                    }
                    tohumSp.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.spinner, list));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {}
            });

            DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVAN_SAHIPLERI);
            Query query1 = ref1.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
            query1.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    hayvanSahipleri = new ArrayList<>();
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        HayvanSahibi hs = ds.getValue(HayvanSahibi.class);

                        if (hs != null) {
                            hs.uid = ds.getKey();
                            hayvanSahipleri.add(hs);
                        }
                    }

                    if (getActivity() != null)
                        HS.setOnClickListener(view -> new HSSelectDialog(getActivity().getSupportFragmentManager(), hayvanSahipleri, SearchTohumlamaFragment.this).createDialog());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {}
            });

            basariDurumuSp.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.spinner_centered_text, Utils.BASARI_DURUMLARI));

            baslangicBtn.setOnClickListener(view -> {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR), month = calendar.get(Calendar.MONTH), day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dpd = new DatePickerDialog(getActivity(), this::onDateSet, year, month, day);
                dpd.getDatePicker().setTag(BASLANGIC_TAG);
                dpd.show();
            });
            bitisBtn.setOnClickListener(view -> {
                if (!baslangicKey || selectedStartDate != 0) {
                    Calendar calendar = Calendar.getInstance();
                    int year = calendar.get(Calendar.YEAR), month = calendar.get(Calendar.MONTH), day = calendar.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog dpd = new DatePickerDialog(getActivity(), this::onDateSet, year, month, day);
                    dpd.getDatePicker().setTag(BITIS_TAG);
                    if (baslangicKey)
                        dpd.getDatePicker().setMinDate(selectedStartDate);
                    dpd.show();
                }else Toast.makeText(getActivity(), getString(R.string.error_baslangic_secin), Toast.LENGTH_SHORT).show();
            });
        }
    }

    private void updateUI(String inputType, boolean key) {
        switch (inputType) {
            case Utils.HS_NAME:
                HSKey = key;
                if (key) {
                    HSContainer.setVisibility(View.VISIBLE);
                    filterHS.setVisibility(View.GONE);
                }else {
                    selectedHS = null;
                    HS.setText("");
                    HSContainer.setVisibility(View.GONE);
                    filterHS.setVisibility(View.VISIBLE);
                }
                break;

            case Utils.TOHUM_ADI:
                tohumKey = key;

                if (key) {
                    tohumContainer.setVisibility(View.VISIBLE);
                    filterTohum.setVisibility(View.GONE);
                }else {
                    tohumSp.setSelection(0);
                    tohumContainer.setVisibility(View.GONE);
                    filterTohum.setVisibility(View.VISIBLE);
                }
                break;

            case Utils.TOHUMLAMA_TARIHI:
                tarihKey = key;

                if (key) {
                    tarihContainer.setVisibility(View.VISIBLE);
                    filterTarih.setVisibility(View.GONE);
                    updateUI(Utils.BASLANGIC_TARIHI, true);
                    updateUI(Utils.BITIS_TARIHI, true);
                }else {
                    tarihContainer.setVisibility(View.GONE);
                    filterTarih.setVisibility(View.VISIBLE);
                }
                break;

            case Utils.BASLANGIC_TARIHI:
                updateBaslangic(key);

                if (!key && !bitisKey)
                    updateUI(Utils.TOHUMLAMA_TARIHI, false);

                if (!key) {
                    selectedStartDate = 0;
                    baslangicBtn.setTextColor(getResources().getColor(R.color.colorAccent));
                    baslangicBtn.setText(R.string.sec);
                }
                break;

            case Utils.BITIS_TARIHI:
                updateBitis(key);

                if (!key && !baslangicKey)
                    updateUI(Utils.TOHUMLAMA_TARIHI, false);

                if (!key) {
                    selectedEndDate = 0;
                    bitisBtn.setTextColor(getResources().getColor(R.color.colorAccent));
                    bitisBtn.setText(R.string.sec);
                }
                break;

            case Utils.BASARI_DURUMU:
                basariDurumuKey = key;

                if (key) {
                    basariDurumuContainer.setVisibility(View.VISIBLE);
                    filterBasariDurumu.setVisibility(View.GONE);
                }else {
                    basariDurumuContainer.setVisibility(View.GONE);
                    filterBasariDurumu.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private void updateBaslangic(boolean key) {
        baslangicKey = key;

        if (key) {
            baslangicTv.setVisibility(View.VISIBLE);
            baslangicBtn.setVisibility(View.VISIBLE);
            closeBaslangic.setVisibility(View.VISIBLE);
            filterBaslangic.setVisibility(View.GONE);
        }else {
            baslangicTv.setVisibility(View.INVISIBLE);
            baslangicBtn.setVisibility(View.GONE);
            closeBaslangic.setVisibility(View.INVISIBLE);
            filterBaslangic.setVisibility(View.VISIBLE);
        }
    }

    private void updateBitis(boolean key) {
        bitisKey = key;

        if (key) {
            bitisTv.setVisibility(View.VISIBLE);
            bitisBtn.setVisibility(View.VISIBLE);
            closeBitis.setVisibility(View.VISIBLE);
            filterBitis.setVisibility(View.GONE);
        }else {
            bitisTv.setVisibility(View.INVISIBLE);
            bitisBtn.setVisibility(View.GONE);
            closeBitis.setVisibility(View.INVISIBLE);
            filterBitis.setVisibility(View.VISIBLE);
        }
    }

    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        Calendar c = Calendar.getInstance();
        c.set(i, i1, i2, 0, 0, 0);
        if (datePicker.getTag().toString().equals(BASLANGIC_TAG)) {
            selectedStartDate = c.getTimeInMillis();

            baslangicBtn.setTextColor(getResources().getColor(R.color.black));
            baslangicBtn.setText(Utils.getDateString(i, i1 + 1, i2));

            if (bitisKey && selectedEndDate != 0) {
                if (selectedStartDate > selectedEndDate)
                    updateUI(Utils.BITIS_TARIHI, false);
            }
        } else if (datePicker.getTag().toString().equals(BITIS_TAG)) {
            Log.d("SearchTohumlamaFragment", "onDateSet: ");
            selectedEndDate = c.getTimeInMillis();
            bitisBtn.setTextColor(getResources().getColor(R.color.black));
            bitisBtn.setText(Utils.getDateString(i, i1 + 1, i2));
        }
    }

    @Override
    public void onHSSelected(HayvanSahibi selected) {
        selectedHS = selected;
        HS.setText(selected.isim);
    }

}