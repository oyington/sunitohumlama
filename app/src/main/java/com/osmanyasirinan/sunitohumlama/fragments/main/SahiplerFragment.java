package com.osmanyasirinan.sunitohumlama.fragments.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.activities.hayvansahibi.HayvanSahibiDetayActivity;
import com.osmanyasirinan.sunitohumlama.activities.hayvansahibi.YeniHayvanSahibiActivity;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class SahiplerFragment extends Fragment {

    ListView lv;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sahipler, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView add = view.findViewById(R.id.addHayvanSahibiBtn);
        add.setOnClickListener(view1 -> {
            Intent i = new Intent(getActivity(), YeniHayvanSahibiActivity.class);
            startActivity(i);
        });

        lv = view.findViewById(R.id.hayvanSahipleriLv);
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVAN_SAHIPLERI);

            Query query = ref.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    List<String> uids = new ArrayList<>();
                    List<String> sahipler = new ArrayList<>();
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        HayvanSahibi hs = ds.getValue(HayvanSahibi.class);

                        if (hs != null) {
                            uids.add(ds.getKey());
                            sahipler.add(hs.isim);
                            // TODO: Profile Photo
                        }
                    }

                    if (getActivity() != null) {
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.hayvan_sahibi, R.id.hayvanSahibiTV, sahipler);
                        lv.setAdapter(adapter);
                        lv.setOnItemClickListener((adapterView, view12, i, l) -> {
                            Intent intent = new Intent(getActivity(), HayvanSahibiDetayActivity.class);
                            intent.putExtra(Utils.EXTRA_HSUID, uids.get(i));
                            startActivity(intent);
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}