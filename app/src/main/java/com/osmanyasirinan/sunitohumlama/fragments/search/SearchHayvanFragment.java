package com.osmanyasirinan.sunitohumlama.fragments.search;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.activities.main.FilteredListActivity;
import com.osmanyasirinan.sunitohumlama.data.HayvanSahibi;
import com.osmanyasirinan.sunitohumlama.dialogs.main.HSSelectDialog;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class SearchHayvanFragment extends Fragment implements Utils.HSListener {

    private EditText hayvanBilgisi, HS;
    private Button addFilter, submit;
    private LinearLayout HSContainer;
    private ImageView close;
    private HayvanSahibi selectedHS;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_hayvan, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        hayvanBilgisi = view.findViewById(R.id.searchHayvanBilgisi);
        HS = view.findViewById(R.id.searchHayvanHS);
        HSContainer = view.findViewById(R.id.searchHayvanHSContainer);
        addFilter = view.findViewById(R.id.searchHayvanAddFilter);
        submit = view.findViewById(R.id.searchInHayvan);
        close = view.findViewById(R.id.closeHayvanHS);

        hayvanBilgisi.addTextChangedListener(Utils.watcher(hayvanBilgisi, Utils.HAYVAN_BILGISI));
        HS.addTextChangedListener(Utils.watcher(HS, Utils.HS_NAME));

        close.setOnClickListener(view1 -> {
            HSContainer.setVisibility(View.GONE);
            addFilter.setVisibility(View.VISIBLE);
        });
        addFilter.setOnClickListener(view1 -> {
            HSContainer.setVisibility(View.VISIBLE);
            addFilter.setVisibility(View.GONE);
        });

        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_HAYVAN_SAHIPLERI);
            Query query = ref.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    List<HayvanSahibi> list = new ArrayList<>();
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        HayvanSahibi hs = ds.getValue(HayvanSahibi.class);

                        if (hs != null) {
                            hs.uid = ds.getKey();
                            list.add(hs);
                        }
                    }
                    HS.setOnClickListener(view1 -> {
                        if (getActivity() != null)
                            new HSSelectDialog(getActivity().getSupportFragmentManager(), list, SearchHayvanFragment.this).createDialog();
                    });
                    submit.setOnClickListener(view1 -> submit());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }

    private void submit() {
        Intent i = new Intent(getActivity(), FilteredListActivity.class);
        ArrayList<String> list = new ArrayList<>();
        if (selectedHS != null)
            list.add(Utils.SEARCHF_HAYVAN_SAHIBI_ID, selectedHS.uid);
        else
            list.add(Utils.SEARCHF_HAYVAN_SAHIBI_ID, "");
        list.add(Utils.SEARCHF_HAYVAN_BILGISI, hayvanBilgisi.getText().toString().trim());
        i.putExtra(Utils.EXTRA_SEARCH, Utils.SEARCH_HAYVAN);
        i.putExtra(Utils.EXTRA_SEARCH_FIELDS, list);
        startActivity(i);
    }

    @Override
    public void onHSSelected(HayvanSahibi selected) {
        selectedHS = selected;
        HS.setText(selected.isim);
    }
}
