package com.osmanyasirinan.sunitohumlama.fragments.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.activities.main.SearchActivity;
import com.osmanyasirinan.sunitohumlama.activities.tohumlama.TohumlamaDetayActivity;
import com.osmanyasirinan.sunitohumlama.data.Tohumlama;
import com.osmanyasirinan.sunitohumlama.dialogs.main.NewDialog;
import com.osmanyasirinan.sunitohumlama.dialogs.main.SearchDialog;
import com.osmanyasirinan.sunitohumlama.utilities.HomeListAdapter;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private ImageView search, add;
    private ListView list;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // BUTTONS

        search = view.findViewById(R.id.searchTohumlamaButton);
        add = view.findViewById(R.id.addTohumlamaButton);

        search.setOnClickListener(view1 -> {
            if (getActivity() != null)
                new SearchDialog(getActivity().getSupportFragmentManager()).createDialog();
        });

        add.setOnClickListener(view1 -> {
            if (getActivity() != null)
                new NewDialog(getActivity().getSupportFragmentManager()).createDialog();
        });

        // LISTVIEW

        list = view.findViewById(R.id.sonTohumlamalarLv);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        List<Tohumlama> tohumlamalar = new ArrayList<>();

        if (mAuth.getCurrentUser() != null && getActivity() != null) {
            ProgressDialog dialog = new ProgressDialog(getActivity());
            dialog.setMessage(getString(R.string.info_please_wait));
            dialog.show();

            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAMALAR);
            Query query = ref.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        Tohumlama t = ds.getValue(Tohumlama.class);

                        if (t != null) {
                            t.setId(ds.getKey());
                            tohumlamalar.add(t);
                        }
                    }

                    if (getActivity() != null) {
                        HomeListAdapter adapter = new HomeListAdapter(getActivity(), tohumlamalar);
                        list.setAdapter(adapter);
                    }
                    dialog.dismiss();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        
        list.setOnItemClickListener((adapterView, view1, i, l) -> {
            Intent in = new Intent(getActivity(), TohumlamaDetayActivity.class);
            in.putExtra(Utils.EXTRA_TOHUMLAMAID, tohumlamalar.get(i).getId());
            startActivity(in);
        });
    }

}