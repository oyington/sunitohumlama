package com.osmanyasirinan.sunitohumlama.fragments.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.activities.main.UpdateProfileActivity;
import com.osmanyasirinan.sunitohumlama.auth.LoginActivity;
import com.osmanyasirinan.sunitohumlama.data.VeterinerHekim;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

public class SettingsFragment extends Fragment {

    private FirebaseAuth mAuth;
    private ImageView logout;
    private TextView nameTV, accountTypeTV, emailTV, provinceTV;
    private Button edit;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAuth = FirebaseAuth.getInstance();

        logout = view.findViewById(R.id.logoutBtn);
        logout.setOnClickListener(view1 -> {
            if (getActivity() != null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                        .setMessage(getString(R.string.sure_logout))
                        .setPositiveButton(getString(R.string.yes), (dialogInterface, i) -> logout())
                        .setNegativeButton(getString(R.string.no), null);
                builder.show();
            }
        });

        provinceTV = view.findViewById(R.id.settingsProvinceTv);
        nameTV = view.findViewById(R.id.settingsNameTv);
        accountTypeTV = view.findViewById(R.id.settingsAccountTypeTv);
        emailTV = view.findViewById(R.id.settingsEmailTv);
        edit = view.findViewById(R.id.settingsEditBtn);

        edit.setOnClickListener(view1 -> {
            Intent i = new Intent(getActivity(), UpdateProfileActivity.class);
            startActivity(i);
        });

        fill();
    }

    private void logout(){
        if (mAuth.getCurrentUser() != null && getActivity() != null) {
            mAuth.signOut();
            Intent i = new Intent(getActivity(), LoginActivity.class);
            startActivity(i);
            getActivity().finish();
        }
    }

    private void fill() {
        if (mAuth.getCurrentUser() != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_UYELER);
            ref.child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    VeterinerHekim vh = snapshot.getValue(VeterinerHekim.class);

                    if (getActivity() != null && vh != null) {
                        provinceTV.setText(Utils.getUserProvince(getActivity()));
                        nameTV.setText(vh.name);
                        accountTypeTV.setText(R.string.veteriner);
                        emailTV.setText(vh.email);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }

}