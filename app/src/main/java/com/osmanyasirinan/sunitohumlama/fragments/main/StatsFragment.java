package com.osmanyasirinan.sunitohumlama.fragments.main;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.Tohum;
import com.osmanyasirinan.sunitohumlama.data.Tohumlama;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class StatsFragment extends Fragment {

    private List<Tohum> tohumlar;
    private FirebaseAuth mAuth;

    private Spinner basariOraniSpinner;
    private ProgressBar basariOraniBar;
    private TextView basariOraniTv;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stats, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        basariOraniSpinner = view.findViewById(R.id.statsBasariOraniSpinner);
        basariOraniBar = view.findViewById(R.id.statsBasariOraniProgressBar);
        basariOraniTv = view.findViewById(R.id.statsBasariOraniYuzdeTv);
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onResume() {
        super.onResume();


        if (mAuth.getCurrentUser() != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAR);
            Query query = ref.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    tohumlar = new ArrayList<>();
                    List<String> tohumIsimleri = new ArrayList<>();

                    for (DataSnapshot ds : snapshot.getChildren()) {
                        Tohum t = ds.getValue(Tohum.class);

                        if (t != null) {
                            t.id = ds.getKey();
                            tohumlar.add(t);
                            tohumIsimleri.add(t.isim);
                        }
                    }

                    if (getActivity() != null)
                        basariOraniSpinner.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.spinner, tohumIsimleri));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

            basariOraniSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAMALAR);
                    Query query = ref.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
                    query.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            int basarisiz = 0, basarili = 0;

                            for (DataSnapshot ds : snapshot.getChildren()) {
                                Tohumlama t = ds.getValue(Tohumlama.class);

                                if (t != null) {
                                    if (t.getAtilanTohumID().equals(tohumlar.get(i).id)) {
                                        if (t.getBasariDurumu() == 1)
                                            basarili++;
                                        else if (t.getBasariDurumu() == -1)
                                            basarisiz++;
                                    }
                                }
                            }

                            int yuzde = Math.round(((float) basarili / (basarili + basarisiz)) * 100);
                            basariOraniBar.setProgress(yuzde, true);
                            String yuzdeStr = "%" + yuzde;
                            basariOraniTv.setText(yuzdeStr);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }
}