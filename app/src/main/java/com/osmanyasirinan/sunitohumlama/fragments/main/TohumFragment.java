package com.osmanyasirinan.sunitohumlama.fragments.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.data.Tohum;
import com.osmanyasirinan.sunitohumlama.dialogs.tohum.TohumDetayDialog;
import com.osmanyasirinan.sunitohumlama.dialogs.tohum.YeniTohumDialog;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class TohumFragment extends Fragment {

    private ListView lv;
    private ImageView add;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tohum, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lv = view.findViewById(R.id.tohumlarLv);
        add = view.findViewById(R.id.addTohumButton);

        add.setOnClickListener(view1 -> {
            if (getActivity() != null)
                new YeniTohumDialog(getActivity().getSupportFragmentManager()).showDialog();
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Utils.TABLO_TOHUMLAR);

            Query query = ref.orderByChild("uyeID").equalTo(mAuth.getCurrentUser().getUid());
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    List<Tohum> tohumlar = new ArrayList<>();
                    List<String> tohumIsimleri = new ArrayList<>();

                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        Tohum t = ds.getValue(Tohum.class);

                        if (t != null) {
                            t.id = ds.getKey();
                            tohumlar.add(t);
                            tohumIsimleri.add(t.isim);
                        }
                    }

                    if (getActivity() != null) {
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.tohum, R.id.tohumTv, tohumIsimleri);
                        lv.setAdapter(adapter);
                        lv.setOnItemClickListener(((adapterView, view, i, l) -> new TohumDetayDialog(getActivity().getSupportFragmentManager(), tohumlar.get(i)).showDialog()));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }
}