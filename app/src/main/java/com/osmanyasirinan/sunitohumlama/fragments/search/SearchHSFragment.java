package com.osmanyasirinan.sunitohumlama.fragments.search;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.osmanyasirinan.sunitohumlama.R;
import com.osmanyasirinan.sunitohumlama.activities.main.FilteredListActivity;
import com.osmanyasirinan.sunitohumlama.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchHSFragment extends Fragment {

    private AutoCompleteTextView province;
    private Spinner town;
    private EditText name, mahallekoy;
    private ImageView closeProvince, closeTown, closeMahalleKoy;
    private Button addFilter, submit;
    private LinearLayout provinceContainer, townContainer, mahallekoyContainer;

    private boolean provinceKey, townKey, mahallekoyKey;
    private int shownInputs;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_hs, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        name = view.findViewById(R.id.searchHSName);
        province = view.findViewById(R.id.searchHSProvince);
        town = view.findViewById(R.id.searchHSTown);
        mahallekoy = view.findViewById(R.id.searchHSMahalleKoy);
        closeProvince = view.findViewById(R.id.closeHSProvince);
        closeTown = view.findViewById(R.id.closeHSTown);
        closeMahalleKoy = view.findViewById(R.id.closeHSMahallekoy);
        addFilter = view.findViewById(R.id.searchHSAddFilter);
        submit = view.findViewById(R.id.searchInHS);
        provinceContainer = view.findViewById(R.id.searchHSProvinceContainer);
        townContainer = view.findViewById(R.id.searchHSTownContainer);
        mahallekoyContainer = view.findViewById(R.id.searchHSMahallekoyContainer);

        province.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().trim().isEmpty())
                    Utils.setDrawables(province, Utils.getIconID(Utils.PROVINCE, false));
                else
                    Utils.setDrawables(province, Utils.getIconID(Utils.PROVINCE, true));
                close(2, 1);
            }
        });
        name.addTextChangedListener(Utils.watcher(name, Utils.HS_NAME));
        mahallekoy.addTextChangedListener(Utils.watcher(mahallekoy, Utils.MAHALLEKOY));

        province.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.spinner, Utils.SEHIRLER));

        closeProvince.setOnClickListener(view1 -> close(3));
        closeTown.setOnClickListener(view1 -> close(2));
        closeMahalleKoy.setOnClickListener(view1 -> close(1));

        provinceKey = false;
        townKey = false;
        mahallekoyKey = false;
        shownInputs = 0;

        submit.setOnClickListener(view1 -> submit());
        addFilter.setOnClickListener(view1 -> addFilter());
    }

    private void close(int count) {
        if (count == 3) {
            provinceContainer.setVisibility(View.GONE);
            provinceKey = false;
        }

        if (count >= 2) {
            townContainer.setVisibility(View.GONE);
            townKey = false;
        }

        if (count >= 1) {
            mahallekoyContainer.setVisibility(View.GONE);
            mahallekoyKey = false;
        }

        addFilter.setVisibility(View.VISIBLE);
        shownInputs--;
    }

    private void close(int count, int shownInputs) {
        if (count == 3) {
            provinceContainer.setVisibility(View.GONE);
            provinceKey = false;
        }

        if (count >= 2) {
            townContainer.setVisibility(View.GONE);
            townKey = false;
        }

        if (count >= 1) {
            mahallekoyContainer.setVisibility(View.GONE);
            mahallekoyKey = false;
        }

        addFilter.setVisibility(View.VISIBLE);
        this.shownInputs = shownInputs;
    }

    private void addFilter() {
        switch (shownInputs) {
            case 0:
                provinceContainer.setVisibility(View.VISIBLE);
                provinceKey = true;
                shownInputs++;
                break;

            case 1:
                if (Utils.checkInput(Utils.PROVINCE, province.getText().toString().trim())) {
                    townContainer.setVisibility(View.VISIBLE);
                    townKey = true;
                    try {
                        JSONObject towns = Utils.getTowns(getActivity());

                        if (towns != null) {
                            JSONArray jsonArray = (JSONArray) towns.get(province.getText().toString().trim());

                            List<String> list = new ArrayList<>();
                            list.add(getString(R.string.town));

                            for (int i = 0; i < jsonArray.length(); i++)
                                list.add(jsonArray.get(i).toString());

                            town.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.spinner, list));
                            shownInputs++;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else Toast.makeText(getActivity(), getString(R.string.error_province), Toast.LENGTH_SHORT).show();
                break;

            case 2:
                if (town.getSelectedItemPosition() != 0) {
                    mahallekoyContainer.setVisibility(View.VISIBLE);
                    mahallekoyKey = true;
                    addFilter.setVisibility(View.GONE);
                    shownInputs++;
                }else Toast.makeText(getActivity(), getString(R.string.error_town), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void submit() {
        Intent i = new Intent(getActivity(), FilteredListActivity.class);
        ArrayList<String> list = new ArrayList<>();

        list.add(Utils.SEARCHF_HS_NAME, name.getText().toString().trim());

        String provinceStr = province.getText().toString().trim();

        if (provinceKey) {
            if (Utils.checkInput(Utils.PROVINCE, provinceStr)) {
                list.add(Utils.SEARCHF_PROVINCE, provinceStr);

                if (townKey) {
                    String townStr = town.getSelectedItem().toString();
                    if (townStr.isEmpty() && townStr.equals(getString(R.string.town)) && !mahallekoy.getText().toString().trim().isEmpty())
                        Toast.makeText(getActivity(), getString(R.string.error_town), Toast.LENGTH_SHORT).show();
                    else {
                        list.add(Utils.SEARCHF_TOWN, (!townStr.equals(getString(R.string.town))) ? townStr : "");

                        if (mahallekoyKey)
                            list.add(Utils.SEARCHF_MAHALLEKOY, mahallekoy.getText().toString().trim());
                        else
                            list.add(Utils.SEARCHF_MAHALLEKOY, "");
                    }
                } else {
                    list.add(Utils.SEARCHF_TOWN, "");
                    list.add(Utils.SEARCHF_MAHALLEKOY, "");
                }
                i.putExtra(Utils.EXTRA_SEARCH, Utils.SEARCH_HS);
                i.putExtra(Utils.EXTRA_SEARCH_FIELDS, list);
                startActivity(i);
            }else
                Toast.makeText(getActivity(), getString(R.string.invalid_province), Toast.LENGTH_SHORT).show();
        } else {
            list.add(Utils.SEARCHF_PROVINCE, "");
            list.add(Utils.SEARCHF_TOWN, "");
            list.add(Utils.SEARCHF_MAHALLEKOY, "");

            i.putExtra(Utils.EXTRA_SEARCH, Utils.SEARCH_HS);
            i.putExtra(Utils.EXTRA_SEARCH_FIELDS, list);
            startActivity(i);
        }
    }
}
